<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del proveedor
$consulta = $conexion->query("SELECT * FROM proveedores WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i a', strtotime($fila['fecha']));
    $usuario = $fila['usuario'];
    $proveedor = $fila['proveedor'];
    $correo = $fila['correo'];
    $telefono = $fila['telefono'];
    $imagen = $fila['imagen'];
    $imagen_nombre = $fila['imagen_nombre'];    
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="proveedores_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Proveedor</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este proveedor</h2>
                <form action="proveedores_detalle.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>" />
                    <input type="hidden" name="imagen" value="<?php echo "$imagen"; ?>" />
                    <input type="hidden" name="imagen_nombre" value="<?php echo "$imagen_nombre"; ?>" />
                    <p><label for="proveedor">Proveedor:</label></p>
                    <p><input type="text" id="proveedor" name="proveedor" value="<?php echo "$proveedor"; ?>" required autofocus /></p>
                    <p><label for="correo">Correo:</label></p>
                    <p><input type="email" id="correo" name="correo" value="<?php echo "$correo"; ?>" /></p>
                    <p><label for="telefono">Teléfono:</label></p>
                    <p><input type="tel" id="telefono" name="telefono" value="<?php echo "$telefono"; ?>" required /></p>                    
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="proveedores_ver.php?eliminar=si&id=<?php echo "$id"; ?>&proveedor=<?php echo "$proveedor"; ?>"><input type="button" class="advertencia" value="Eliminar este proveedor"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>