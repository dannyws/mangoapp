<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head <meta http-equiv="refresh" content="20" >
    ?>    
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="logueo_salir.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt">Salir</span></h2>
                </div>
            </a>
            <a href="index.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ajustes.php">
                <div class="cabezote_col_der">
                    <?php
                    //le doy acceso al modulo segun el perfil que tenga
                    if (($sesion_tipo == "administrador") or ($sesion_tipo == "socio"))
                    {

                    ?>
                    <h2><span class="logo_txt">Ajustes</span></div></h2>
                    <?php
                    }
                    ?>
                </div>
            </a>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba" style="background-image: url('<?php echo ($sesion_imagen) ?>');"></div>
            <h2 class="cab_texto"><?php echo ucwords($sesion_nombres) ?> <?php echo ucwords($sesion_apellidos) ?></h2>
            <div class="bloque_margen">
                <h2>Inicio</h2>
                <p>Hola! este es el <strong>Inicio</strong>, acá puedes tener acceso a todos los módulos de ManGo! para que puedas administrar tu negocio con la punta de un solo dedo.</p>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Tu información</h2>
                <p><span class="item_titulo">Tipo</span> <?php echo ucfirst($sesion_tipo);?></p>
                <p><span class="item_titulo">Correo</span> <?php echo "$sesion_correo";?></p>
                <p><span class="item_titulo">Local al que estás relacionado</span> <?php echo ucfirst($sesion_local);?> (<?php echo ucfirst($sesion_local_tipo);?>)</p>                                
            </div>
        </article>
        
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Ventas</h2>
                <p>Acá puedes abrir el módulo de ventas que se usará desde un teléfono móvil, tableta o computador para que tus asesores, meseros o vendedores puedan vender tus productos y servicios</p>
                <p class="alineacion_botonera"><a href="ventas_ubicaciones.php"><input type="button" class="proceder" value="Nueva venta"></a></p>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Zonas de entregas</h2>
                <p>Las zonas de entrega son los distintos lugares a donde llegan los pedidos de productos o servicios que se generan en una venta, por ejemplo: la comida se entrega en la cocina, las carnes en la parrilla, los licores en el bar, las camisas en la bodega, etc.</p>
                <p class="alineacion_botonera"><a href="zonas_entregas_entrada.php"><input type="button" class="proceder" value="Ver Zonas"></a></p>
            </div>
        </article>
        
        <?php
        //le doy acceso al modulo segun el perfil que tenga
        if (($sesion_tipo == "administrador") or ($sesion_tipo == "socio"))
        {

        ?>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Despachos</h2>
                <p>Acá puedes abrir el módulo de despachos con el que podrás enviar los componentes de los que están hechos tus productos y servicios a tus distintos locales o puntos de venta.</p>
                <p class="alineacion_botonera"><a href="despachos_crear.php"><input type="button" class="proceder" value="Nuevo despacho"></a></p>
            </div>
        </article>

        <?php 
        }
        ?>        

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Inventario</h2>
                <p>Acá puedes abrir el módulo de inventario con el que podrás recibir los despachos de componentes y visualizar su cantidad.</p>
                <p class="alineacion_botonera"><a href="inventario.php"><input type="button" class="proceder" value="Ver inventario"></a></p>
            </div>
        </article>       

        <?php
        //le doy acceso al modulo segun el perfil que tenga
        if (($sesion_tipo == "administrador") or ($sesion_tipo == "socio"))
        {

        ?>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Reportes</h2>
                <p>Acá puedes abrir el módulo de reportes para consultar los ingresos, produtos mas vendidos, tipos de pagos, cierres de día, etc.</p>
                <p class="alineacion_botonera"><a href="reportes.php"><input type="button" class="proceder" value="Consultar"></a></p>
            </div>
        </article>

        <?php 
        }
        ?>

        <?php
        //le doy acceso al modulo segun el perfil que tenga
        if (($sesion_tipo == "administrador") or ($sesion_tipo == "socio"))
        {

        ?>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Ingresos</h2>
                <?php
                //ingresos de hoy
                $fecha_inicio_hoy = date('Y-m-d 00:00:00');
                $fecha_fin_hoy = date('Y-m-d 23:59:59');

                $consulta_ingresos_hoy = $conexion->query("SELECT * FROM ventas_datos WHERE fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy'");        

                $total_dia_hoy = 0;

                while ($fila_ingresos_hoy = $consulta_ingresos_hoy->fetch_assoc())
                {
                    $total_neto_hoy = $fila_ingresos_hoy['total_neto'];
                    $total_dia_hoy = $total_dia_hoy + $total_neto_hoy;      
                }
                ?>

                <?php
                //ingresos de ayer
                $fecha_inicio_ayer = date('Y-m-d 00:00:00', strtotime("yesterday"));
                $fecha_fin_ayer = date('Y-m-d 23:59:59', strtotime("yesterday"));

                $consulta_ingresos_ayer = $conexion->query("SELECT * FROM ventas_datos WHERE fecha BETWEEN '$fecha_inicio_ayer' and '$fecha_fin_ayer'");        

                $total_dia_ayer = 0;

                while ($fila_ingresos_ayer = $consulta_ingresos_ayer->fetch_assoc())
                {
                    $total_neto_ayer = $fila_ingresos_ayer['total_neto'];
                    $total_dia_ayer = $total_dia_ayer + $total_neto_ayer;      
                }                
                ?>

                <?php
                //porcentaje de crecimiento                
                if ($total_dia_ayer == 0)
                {
                   $porcentaje_crecimiento = "<span class='texto_neutro'>Ayer no hubo ventas</span>";
                }
                else
                {
                    //$porcentaje_crecimiento = ($total_dia_hoy - $total_dia_ayer) / $total_dia_ayer * 100;
                    $porcentaje_crecimiento = ($total_dia_hoy - $total_dia_ayer) / $total_dia_ayer * 100;
                    $porcentaje_crecimiento = number_format($porcentaje_crecimiento, 0, ".", ".");
                    $total_dia_ayer = "$" . number_format($total_dia_ayer, 0, ".", ".");
                    

                    if ($porcentaje_crecimiento > 1)
                    {
                        $porcentaje_crecimiento = "<div class='flecha_arr'></div> <span class='texto_exito'>$porcentaje_crecimiento% Día anterior ($total_dia_ayer)</span>";
                    }
                    else
                    {
                        $porcentaje_crecimiento = "<div class='flecha_aba'></div> <span class='texto_error'>$porcentaje_crecimiento% Día anterior ($total_dia_ayer)</span>";
                    }
                }

                if ($total_dia_hoy == 0)
                {
                   $porcentaje_crecimiento = "<span class='texto_neutro'>Aún no hay ventas</span>";
                }
                ?>

                <p class="ingresos_texto">$<?php echo number_format($total_dia_hoy, 0, ",", ".");?></p>
                <h3><?php echo "$porcentaje_crecimiento";?></h3>

                <?php
                //ventas por locales
                $consulta = $conexion->query("SELECT count(local_id), local_id FROM ventas_datos WHERE fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy' GROUP BY local_id ORDER BY count(local_id) ASC");                

                

                while ($fila = $consulta->fetch_assoc())
                {
                    $local = $fila['local_id'];

                    //consulto el total para cada local
                    $consulta2 = $conexion->query("SELECT * FROM ventas_datos WHERE local_id = '$local' and fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy'");       

                    $total_local = 0;
                    while ($fila2 = $consulta2->fetch_assoc())
                    {
                        $total_neto = $fila2['total_neto'];
                        $total_local = $total_local + $total_neto;
                        $total_local_t = "$" . number_format($total_local, 0, ".", ".");
                    }
                   
                    //consulto el nombre del local
                    $consulta3 = $conexion->query("SELECT * FROM locales WHERE id = '$local'");
                    while ($fila3 = $consulta3->fetch_assoc())
                    {
                        $local = $fila3['local'];
                    }

                    $porcentaje_local = ($total_local / $total_dia_hoy) * 100;
                    $porcentaje_local = number_format($porcentaje_local, 0, ".", ".") . "%";

                    ?>

                    <div class="barra_fondo">
                        <div class="barra_porcentaje_margen">
                            <div class="barra_porcentaje" style="width: <?php echo "$porcentaje_local"; ?>">                                
                            </div>
                        </div>
                    </div>

                    <p class="texto_porcentaje">
                        <span class="porcentaje_izq"><?php echo ucwords($local); ?></span>
                        <span class="porcentaje_der"><?php echo "$total_local_t"; ?></span>
                    </p>

                    <?php
                }
                
                ?>

               <img src="grafico_ventas_dias.php?sesion_local_id=<?php echo "$sesion_local_id";?>">
            </div>
        </article>

        <?php 
        }
        ?>

        <?php
        //le doy acceso al modulo segun el perfil que tenga
        if (($sesion_tipo == "administrador") or ($sesion_tipo == "socio"))
        {

        ?>        

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Ventas por hora para hoy</h2>
                <img src="grafico_horas.php?sesion_local_id=<?php echo "$sesion_local_id";?>">
            </div>
        </article>

        <?php 
        }
        ?>        

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Productos más vendidos hoy</h2>                
                <?php
                //total de productos
                $fecha_inicio_hoy = date("Y-m-d 00:00:00");
                $fecha_fin_hoy = date("Y-m-d 23:59:59");

                $consulta_ingresos_hoy = $conexion->query("SELECT * FROM ventas_productos WHERE fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy'");
                $total_productos = $consulta_ingresos_hoy->num_rows;
                ?>

                <?php
                //ventas por cada producto
                $consulta = $conexion->query("SELECT count(producto), producto FROM ventas_productos WHERE fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy' GROUP BY producto ORDER BY count(producto) DESC");                

                while ($fila = $consulta->fetch_assoc())
                {
                    $producto = $fila['producto'];

                    //consulto el total para cada producto
                    $consulta2 = $conexion->query("SELECT * FROM ventas_productos WHERE producto = '$producto' and fecha BETWEEN '$fecha_inicio_hoy' and '$fecha_fin_hoy'");
                    $total_producto = $consulta2->num_rows;
                    

                    $porcentaje_producto = ($total_producto / $total_productos) * 100;
                    $porcentaje_producto = number_format($porcentaje_producto, 0, ".", ".") . "%";

                    ?>

                    <div class="barra_fondo">
                        <div class="barra_porcentaje_margen">
                            <div class="barra_porcentaje" style="width: <?php echo "$porcentaje_producto"; ?>">                                
                            </div>
                        </div>
                    </div>

                    <p class="texto_porcentaje">
                        <span class="porcentaje_izq"><?php echo ucwords($producto); ?></span>
                        <span class="porcentaje_der"><?php echo "$total_producto"; ?></span>
                    </p>

                    <?php
                }
                
                ?>
               
            </div>
        </article>

    </section>
<!--
    <footer>
        <div class="footer_contenedor">
            <a href="">
                <div class="pie_col_izq">
                    <h2><span class="logo_txt"></span></h2>
                </div>
            </a>
            <a href="">
                <div class="pie_col_cen">
                    <h2><span class="logo_txt">Texto</span></h2>
                </div>
            </a>
            <a href="ajustes.php">
                <div class="pie_col_der">
                    
                </div>
            </a>
        </div>
    </footer>
    -->

</body>
</html>