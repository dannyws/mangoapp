<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$zona = isset($_POST['zona']) ? $_POST['zona'] : null ;
?>

<?php
//agregar la zona de entregas
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM zonas_entregas WHERE zona = '$zona'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO zonas_entregas values ('', '$ahora', '$sesion_id', '$zona')");
        $mensaje = "<p class='mensaje_exito'>La zona de entregas <strong>$zona</strong> fue agregada exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>La zona de entregas <strong>$zona</strong> ya existe, no es posible agregarla de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="zonas_entregas_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Z. Entregas</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar una nueva zona de entregas</h2>
                <?php echo "$mensaje"; ?>                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <p><label for="zona">Zona de entregas:</label></p>
                    <p><input type="text" id="zona" name="zona" required autofocus /></p>                    
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>