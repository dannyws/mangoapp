<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;
$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$descuento = isset($_POST['descuento']) ? $_POST['descuento'] : null ;
$porcentaje = isset($_POST['porcentaje']) ? $_POST['porcentaje'] : null ;
?>

<?php
//actualizo la información del descuento
if ($editar == "si")
{
    $actualizar = $conexion->query("UPDATE descuentos SET fecha = '$ahora', usuario = '$sesion_id', descuento = '$descuento', porcentaje = '$porcentaje' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El descuento <strong>$descuento</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el descuento.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="descuentos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Descuentos</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">        
            
            <?php
            //consulto y muestro el impuesto
            $consulta = $conexion->query("SELECT * FROM descuentos WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Descuento eliminado</h2>
                    <p class="mensaje_error">Esta descuento ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $descuento = $fila['descuento'];
                    $porcentaje = $fila['porcentaje'];

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('img/iconos/descuentos.jpg');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$descuento"); ?></h2>
                    <div class="bloque_margen">                    
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Porcentaje</span><?php echo ucfirst($porcentaje) ?> %</p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="descuentos_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este descuento"></a></p>
                    </div>
                      
                    <?php
                }
            }
            ?>  
            
        </article>
    </section>
    <footer></footer>
</body>
</html>