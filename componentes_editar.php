<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del componente
$consulta = $conexion->query("SELECT * FROM componentes WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $unidad = $fila['unidad'];
    $componente = $fila['componente'];
    $precio_unidad = $fila['precio_unidad'];
    $proveedor = $fila['proveedor'];

    //consulto el proveedor
    $consulta_proveedor = $conexion->query("SELECT * FROM proveedores WHERE id = '$proveedor'");           

    if ($fila = $consulta_proveedor->fetch_assoc()) 
    {
        $proveedor_id = $fila['id'];
        $proveedor = $fila['proveedor'];
    }
    else
    {
        $proveedor_id = "";
        $proveedor = "No se ha asignado un proveedor";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="componentes_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Componente</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este componente</h2>
                <form action="componentes_detalle.php" method="post">
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                    <p><label for="proveedor">Proveedor:</label></p>
                    <p><select id="proveedor" name="proveedor" required>
                        <option value="<?php echo "$proveedor_id"; ?>"><?php echo ucfirst($proveedor) ?></option>
                        <option value=""></option>
                    <?php
                        //consulto y muestro los proveedores
                        $consulta = $conexion->query("SELECT * FROM proveedores ORDER BY proveedor");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_proveedor = $fila['id'];
                                $proveedor = $fila['proveedor'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_proveedor"; ?>"><?php echo ucfirst($proveedor) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado proveedores</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="componente">Nombre del componente:</label></p>
                    <p><input type="text" id="componente" name="componente" value="<?php echo "$componente"; ?>" required /></p>
                    <p><label for="unidad">Unidad de medida:</label></p>
                    <p><select id="unidad" name="unidad" required>
                        <option value="<?php echo "$unidad"; ?>"><?php echo "$unidad"; ?></option>
                        <option value=""></option>
                        <option ="cm">cm</option>
                        <option ="cm2">cm2</option>
                        <option ="cm3">cm3</option>
                        <option ="gr">gr</option>
                        <option ="kg">kg</option>
                        <option ="lb">lb</option>
                        <option ="lt">lt</option>
                        <option ="ml">ml</option>
                        <option ="mt">mt</option>
                        <option ="mt2">mt2</option>
                        <option ="mt3">mt3</option>
                        <option ="tn">tn</option>
                        <option ="unid">unid</option>
                    </select></p>
                    <p><label for="precio_unidad">Precio unitario:</label></p>
                    <p><input type="number" id="precio_unidad" name="precio_unidad" value="<?php echo "$precio_unidad"; ?>" required /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="componentes_ver.php?eliminar=si&id=<?php echo "$id"; ?>&componente=<?php echo "$componente"; ?>"><input type="button" class="advertencia" value="Eliminar este componente"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>