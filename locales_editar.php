<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del local
$consulta = $conexion->query("SELECT * FROM locales WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i a', strtotime($fila['fecha']));
    $usuario = $fila['usuario'];
    $local = $fila['local'];
    $direccion = $fila['direccion'];
    $telefono = $fila['telefono'];
    $tipo = $fila['tipo'];
    $imagen = $fila['imagen'];
    $imagen_nombre = $fila['imagen_nombre'];
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="locales_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Local</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este local</h2>
                <form action="locales_detalle.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>" />
                    <input type="hidden" name="imagen" value="<?php echo "$imagen"; ?>" />
                    <input type="hidden" name="imagen_nombre" value="<?php echo "$imagen_nombre"; ?>" />
                    <p><label for="local">Local:</label></p>
                    <p><input type="text" id="local" name="local" value="<?php echo "$local"; ?>" required autofocus /></p>
                    <p><label for="direccion">Dirección:</label></p>
                    <p><input type="text" id="direccion" name="direccion" value="<?php echo "$direccion"; ?>" required /></p>
                    <p><label for="telefono">Teléfono:</label></p>
                    <p><input type="tel" id="telefono" name="telefono" value="<?php echo "$telefono"; ?>" required /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst($tipo) ?></option>
                        <option value=""></option>
                        <option value="bodega">Bodega</option>
                        <option value="punto de venta">Punto de venta</option>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="locales_ver.php?eliminar=si&id=<?php echo "$id"; ?>&local=<?php echo "$local"; ?>"><input type="button" class="advertencia" value="Eliminar este local"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>