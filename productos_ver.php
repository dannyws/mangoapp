<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$producto = isset($_GET['producto']) ? $_GET['producto'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el producto
if ($eliminar == 'si')
{
    $borrar = $conexion->query("DELETE FROM productos WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue eliminado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el producto <strong>$correo</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#portafolio"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/productos.jpg');"></div>
            <h2 class="cab_texto">Productos</h2>
            <div class="bloque_margen">
                <p>Los productos son todos los artículos o servicios que vendes en tu negocio, estos estarán divididos en las categorías que crees, por ejemplo: gaseosa pequeña, hamburguesa mediana, postre de fresa, etc. En esta sección puedes agregar, modificar y eliminar los productos y servicios.</p>
                <p class="alineacion_botonera"><a href="productos_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo producto"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Productos agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un producto" /></p>                    
                </form>
                
                <?php
                //consulto la categoria previamente para la busqueda
                $consulta0 = $conexion->query("SELECT * FROM productos_categorias WHERE categoria like '%$busqueda%'");

                if ($filas0 = $consulta0->fetch_assoc())
                {
                    $categoria = $filas0['id'];
                }
                else
                {
                    $categoria = "bullshit";
                }

                //consulto y muestro los productos
                $consulta = $conexion->query("SELECT * FROM productos WHERE producto like '%$busqueda%' or precio like '%$busqueda%' or descripcion like '%$busqueda%' or categoria like '%$categoria%' ORDER BY producto");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado productos para esta búsqueda.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca un producto para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $categoria = $fila['categoria'];
                        $producto = $fila['producto'];
                        $precio = $fila['precio'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/productos-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/productos-$id-$imagen_nombre-m.jpg";
                        }

                        //consulto la categoria
                        $consulta2 = $conexion->query("SELECT * FROM productos_categorias WHERE id = $categoria");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $categoria = $filas2['categoria'];
                        }
                        else
                        {
                            $categoria = "No se ha asignado una categoria";
                        }

                        $consulta_composicion = $conexion->query("SELECT * FROM composiciones WHERE producto = '$id'");
                        $total_composicion = $consulta_composicion->num_rows;
                        ?>
                        <a href="productos_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$producto"); ?></span>
                                        <span class="item_descripcion">Categoría: <?php echo ucfirst("$categoria"); ?></span>
                                        <span class="item_descripcion">Precio: $ <?php echo number_format($precio, 0, ",", "."); ?></span>
                                        <span class="item_descripcion">Componentes: <?php echo ucfirst("$total_composicion"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>
        
    </section>
    <footer></footer>
</body>
</html>