<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información de la zona de entregas
$consulta = $conexion->query("SELECT * FROM zonas_entregas WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $zona = $fila['zona'];    
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="zonas_entregas_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Z. de Entrega</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar esta zona de entregas</h2>
                <form action="zonas_entregas_detalle.php" method="post">
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                    <p><label for="zona">Zona de entregas:</label></p>
                    <p><input type="text" id="zona" name="zona" value="<?php echo "$zona"; ?>" required autofocus /></p>                    
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="zonas_entregas_ver.php?eliminar=si&id=<?php echo "$id"; ?>&zona=<?php echo "$zona"; ?>"><input type="button" class="advertencia" value="Eliminar esta zona de entregas"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>