<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="index.php"><div class="flecha_izq"></div> <span class="logo_txt"> Inicio</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/ajustes.jpg');"></div>
            <h2 class="cab_texto">Ajustes</h2>
            <div class="bloque_margen">            
                <p>Puedes ajustar toda la información necesaria para que ManGo! funcione como tu quieras en este lugar. Si es la primera vez que usas ManGo! te recomendamos seguir los ajustes en el siguiente orden…</a></p>
            </div>            
        </article>

        <a id="logistica">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Logística</h2>
                <?php
                //consulto el total de locales agregados
                $consulta = $conexion->query("SELECT * FROM locales");
                $locales = $consulta->num_rows;
                ?>
                <a href="locales_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/locales-m.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Locales <span class='texto_exito'>x <?php echo "$locales"; ?></span></span>
                                <span class="item_descripcion">Los locales son todos los puntos de venta que puedes tener en tu negocio, por ejemplo: punto de venta Bogotá, punto de venta Medellín, punto de venta barrio poblado, punto de venta centro comercial del norte, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de usuarios agregados
                $consulta = $conexion->query("SELECT * FROM usuarios");
                $usuarios = $consulta->num_rows;
                ?>
                <a href="usuarios_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/usuarios-m.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Usuarios <span class='texto_exito'>x <?php echo "$usuarios"; ?></span></span>
                                <span class="item_descripcion">Los usuarios son todas las personas que interactúan en tu negocio, por ejemplo: meseros, vendedores, socios, administradores, etc. Acá podrás configurar sus permisos, datos y asignar el local al cuál están relacionados</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de ubicaciones agregados
                $consulta = $conexion->query("SELECT * FROM ubicaciones");
                $ubicaciones = $consulta->num_rows;
                ?>
                <a href="ubicaciones_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/mesa_libre.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Ubicaciones <span class='texto_exito'>x <?php echo "$ubicaciones"; ?></span></span>
                                <span class="item_descripcion">Las ubicaciones son los distintos lugares desde los cuales puedes atender a tus clientes en un punto de venta todas las ventas siempre comienzan desde una ubicación, por ejemplo: mesas, barras, habitaciones, cajas registradoras, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>                


                <?php
                //consulto el total de zonas de entregas
                $consulta = $conexion->query("SELECT * FROM zonas_entregas");
                $ubicaciones = $consulta->num_rows;
                ?>
                <a href="zonas_entregas_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/zonas_entregas.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Zonas de entregas <span class='texto_exito'>x <?php echo "$ubicaciones"; ?></span></span>
                                <span class="item_descripcion">Las zonas de entrega son los distintos lugares a donde llegan los pedidos de productos o servicios que se generan en una venta, por ejemplo: la comida se entrega en la cocina, las carnes en la parrilla, los licores en el bar, las camisas en la  bodega, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </article>

        <a id="financiero">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Financiero</h2>

                <?php
                //consulto el total de impuestos agregados
                $consulta = $conexion->query("SELECT * FROM impuestos");
                $impuestos = $consulta->num_rows;
                ?>
                <a href="impuestos_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/impuestos.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Impuestos <span class='texto_exito'>x <?php echo "$impuestos"; ?></span></span>
                                <span class="item_descripcion">Los impuestos son todas las obligaciones tributarias que tus productos o servicios generan, por ejemplo: IVA, impuesto gastronómico, impuesto de licores, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de tipos de pagos agregados
                $consulta = $conexion->query("SELECT * FROM tipos_pagos");
                $tipos_pagos = $consulta->num_rows;
                ?>
                <a href="tipos_pagos_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/efectivo.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Tipos de pagos <span class='texto_exito'>x <?php echo "$tipos_pagos"; ?></span></span>
                                <span class="item_descripcion">Los tipos de pagos son los diferentes medios de cambio que recibes en tu negocio cuando tus clientes hacen una compra, por ejemplo: efectivo, tarjeta débito, cheque, canje de servicios, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de descuentos agregados
                $consulta = $conexion->query("SELECT * FROM descuentos");
                $descuentos = $consulta->num_rows;
                ?>
                <a href="descuentos_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/descuentos.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Descuentos <span class='texto_exito'>x <?php echo "$descuentos"; ?></span></span>
                                <span class="item_descripcion">Los descuentos son los valores y porcentajes de regalo que puedes generar para tus clientes cuando ellos hagan una compra, por ejemplo: mitad de precio, hora feliz, cumpleaños, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de plantillas de facturas
                $consulta = $conexion->query("SELECT * FROM facturas_plantillas");
                $ubicaciones = $consulta->num_rows;
                ?>
                <a href="facturas_plantillas_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/facturas_plantillas.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Plantillas de facturas <span class='texto_exito'>x <?php echo "$ubicaciones"; ?></span></span>
                                <span class="item_descripcion">Acá podrás crear el formato de las facturas que generan tus productos y servicios. Puedes modificar su titulo, encabezado, pie de página, razón social, resolución de facturación, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        </article>

        <a id="portafolio">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Portafolio</h2>

                <?php
                //consulto el total de categorias agregados
                $consulta = $conexion->query("SELECT * FROM productos_categorias");
                $productos_categorias = $consulta->num_rows;
                ?>
                <a href="categorias_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/categorias.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Categorías <span class='texto_exito'>x <?php echo "$productos_categorias"; ?></span></span>
                                <span class="item_descripcion">Las categorías son los grupos o familias en los que están divididos los productos o servicios que vendes en tu negocio, por ejemplo: carnes, bebidas, licores, postres, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de productos agregados
                $consulta = $conexion->query("SELECT * FROM productos");
                $productos = $consulta->num_rows;
                ?>
                <a href="productos_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/productos-m.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Productos o servicios <span class='texto_exito'>x <?php echo "$productos"; ?></span></span>
                                <span class="item_descripcion">Los productos son todos los artículos o servicios que vendes en tu negocio, estos estarán divididos en las categorías que crees, por ejemplo: gaseosa pequeña, hamburguesa mediana, postre de fresa, etc.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        </article>

        <a id="inventario">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Inventario</h2>
                <?php
                //consulto el total de proveedores agregados
                $consulta = $conexion->query("SELECT * FROM proveedores");
                $proveedores = $consulta->num_rows;
                ?>
                <a href="proveedores_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/proveedores-m.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Proveedores <span class='texto_exito'>x <?php echo "$proveedores"; ?></span></span>
                                <span class="item_descripcion">Los proveedores son las personas o empresas que te venden los componentes para la elaboración de tus productos o servicios.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <?php
                //consulto el total de componentes agregados
                $consulta = $conexion->query("SELECT * FROM componentes");
                $componentes = $consulta->num_rows;
                ?>
                <a href="componentes_ver.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img_top">
                                <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Componentes <span class='texto_exito'>x <?php echo "$componentes"; ?></span></span>
                                <span class="item_descripcion">Los componentes son todos los elementos de los que están compuestos los productos o servicios que vendes, por ejemplo: una hamburguesa está hecha de pan, carne y lechuga.</span>

                            </div>
                        </div>
                    </div>
                </a>

            </div>
        </article>
        
    </section>
    <footer></footer>
</body>
</html>