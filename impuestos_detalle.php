<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;
$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$impuesto = isset($_POST['impuesto']) ? $_POST['impuesto'] : null ;
$porcentaje = isset($_POST['porcentaje']) ? $_POST['porcentaje'] : null ;
?>

<?php
//actualizo la información del impuesto
if ($editar == "si")
{
    $actualizar = $conexion->query("UPDATE impuestos SET fecha = '$ahora', usuario = '$sesion_id', impuesto = '$impuesto', porcentaje = '$porcentaje' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El impuesto <strong>$impuesto</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el impuesto.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="impuestos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Impuestos</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">
        <article class="bloque">
            
            <?php
            //consulto y muestro el impuesto
            $consulta = $conexion->query("SELECT * FROM impuestos WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Impuesto eliminado</h2>
                    <p class="mensaje_error">Esta ubicación ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $impuesto = $fila['impuesto'];
                    $porcentaje = $fila['porcentaje'];

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('img/iconos/impuestos.jpg');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$impuesto"); ?></h2>
                    <div class="bloque_margen">                        
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Porcentaje</span><?php echo ucfirst($porcentaje) ?> %</p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="impuestos_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este impuesto"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>            
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Productos relacionados a este impuesto</h2>                
                <?php                
                //consulto y muestro los productos
                $consulta = $conexion->query("SELECT * FROM productos WHERE impuesto = '$id' ORDER BY categoria, producto");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado productos relacionados a este impuesto.</p>

                    <?php
                }
                else
                {
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $categoria = $fila['categoria'];
                        $producto = $fila['producto'];
                        $precio = $fila['precio'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        $precio_neto = ($precio * $porcentaje / 100) + $precio;

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/productos-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/productos-$id-$imagen_nombre-m.jpg";
                        }

                        //consulto la categoria
                        $consulta2 = $conexion->query("SELECT * FROM productos_categorias WHERE id = $categoria");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $categoria = $filas2['categoria'];
                        }
                        else
                        {
                            $categoria = "No se ha asignado una categoria";
                        }
                        ?>
                        
                            <div class="item">
                                <div class="item">
                                    <div class="item_img_top">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$producto"); ?></span>
                                        <span class="item_descripcion"><?php echo ucfirst("$categoria"); ?></span>
                                        <span class="item_descripcion">Precio bruto $ <?php echo number_format($precio, 0, ",", "."); ?></span>
                                        <span class="item_descripcion">Precio neto $ <?php echo number_format($precio_neto, 0, ",", "."); ?></span>
                                    </div>
                                </div>
                            </div>
                        
                        <?php
                    }
                }
                ?>
            </div>
        </article>

    </section>
    <footer></footer>
</body>
</html>