<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$zona = isset($_GET['zona']) ? $_GET['zona'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino la zona de entregas
if ($eliminar == "si")
{
    $borrar = $conexion->query("DELETE FROM zonas_entregas WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>La zona de entregas <strong>$zona</strong> fue eliminada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar la zona de entregas <strong>$zona</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#logistica"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/zonas_entregas.jpg');"></div>
            <h2 class="cab_texto">Zonas de entregas</h2>
            <div class="bloque_margen">
                <p>Las zonas de entrega son los distintos lugares a donde llegan los pedidos de productos o servicios que se generan en una venta, por ejemplo: la comida se entrega en la cocina, las carnes en la parrilla, los licores en el bar, las camisas en la  bodega, etc. En esta sección puedes agregar, modificar y eliminar las zonas de entregas.</p>
                <p class="alineacion_botonera"><a href="zonas_entregas_agregar.php"><input type="button" class="proceder" value="Agregar una nueva zona de entregas"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Zonas de entregas agregadas</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar una zona de entregas" /></p>                    
                </form>
                <?php
                //consulto y muestro las zonas de entregas
                $consulta = $conexion->query("SELECT * FROM zonas_entregas WHERE zona like '%$busqueda%' ORDER BY zona");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado zonas de entregas para esta búsqueda.</p>

                    <?php
                }
                else
                {
                    ?>

                    <p>Toca una zona de entregas para verla o editarla.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $zona = $fila['zona'];
                       
                        ?>
                        <a href="zonas_entregas_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/zonas_entregas.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$zona"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>  

    </section>
    <footer></footer>
</body>
</html>