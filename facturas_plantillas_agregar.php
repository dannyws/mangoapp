<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null ;
$titulo = isset($_POST['titulo']) ? $_POST['titulo'] : null ;
$texto_superior = isset($_POST['texto_superior']) ? $_POST['texto_superior'] : null ;
$texto_inferior = isset($_POST['texto_inferior']) ? $_POST['texto_inferior'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
?>

<?php
//agregar la plantilla de factura
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM facturas_plantillas WHERE local = '$local'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO facturas_plantillas values ('', '$ahora', '$sesion_id', '$nombre', '$titulo', '$texto_superior', '$texto_inferior', '$local')");
        $mensaje = "<p class='mensaje_exito'>La plantilla de factura <strong>$nombre</strong> fue agregada exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>La plantilla de factura <strong>$nombre</strong> ya existe, no es posible agregarla de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="facturas_plantillas_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> P. de facturas</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar una nueva plantilla de factura</h2>
                <?php echo "$mensaje"; ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <p><label for="nombre">Nombre de la plantilla:</label></p>
                    <p><input type="text" id="nombre" name="nombre" required autofocus /></p>
                    <p><label for="titulo">Titulo de la factura:</label></p>
                    <p><input type="text" id="titulo" name="titulo" required /></p>                    
                    <p><label for="texto_superior">Texto superior:</label></p>
                    <p><textarea id="texto_superior" name="texto_superior"></textarea></p>
                    <p><label for="texto_inferior">Texto inferior:</label></p>
                    <p><textarea id="texto_inferior" name="texto_inferior"></textarea></p>
                    <p><label for="local">Relacionada al local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value=""></option>
                        <option value="0">Todos los locales</option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>