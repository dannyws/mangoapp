<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;
$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null ;
$titulo = isset($_POST['titulo']) ? $_POST['titulo'] : null ;
$texto_superior = isset($_POST['texto_superior']) ? $_POST['texto_superior'] : null ;
$texto_inferior = isset($_POST['texto_inferior']) ? $_POST['texto_inferior'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
?>

<?php
//actualizo la información de la plantilla de factura
if ($editar == "si")
{
    $actualizar = $conexion->query("UPDATE facturas_plantillas SET fecha = '$ahora', usuario = '$sesion_id', nombre = '$nombre', titulo = '$titulo', texto_superior = '$texto_superior', texto_inferior = '$texto_inferior', local = '$local' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>La plantilla de factura <strong>$nombre</strong> fue modificada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar la plantilla de factura.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="facturas_plantillas_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> P. de facturas</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    
    <section id="contenedor">
    
        <article class="bloque">
            
            <?php
            //consulto y muestro la plantillas de facturas
            $consulta = $conexion->query("SELECT * FROM facturas_plantillas WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Plantilla de factura eliminada</h2>
                    <p class="mensaje_error">Esta plantilla de factura ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $nombre = $fila['nombre'];
                    $titulo = $fila['titulo'];
                    $texto_superior = $fila['texto_superior'];
                    $texto_inferior = $fila['texto_inferior'];
                    $local = $fila['local'];                    

                    //consulto el local
                    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

                    if ($fila = $consulta_local->fetch_assoc()) 
                    {
                        $local = $fila['local'];
                        $local_tipo = $fila['tipo'];
                    }
                    else
                    {
                        $local = "Todos los locales";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('img/iconos/facturas_plantillas.jpg');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$nombre"); ?></h2>
                    <div class="bloque_margen">                    
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Titulo de la factura</span><?php echo ucfirst($titulo) ?></p>
                        <p><span class="item_titulo">Texto superior</span><?php echo nl2br($texto_superior) ?></p>
                        <p><span class="item_titulo">Texto inferior</span><?php echo nl2br($texto_inferior) ?></p>
                        <p><span class="item_titulo">Relacionada al local</span><?php echo ucfirst($local) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="facturas_plantillas_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar esta ubicación"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>  
                   
        </article>        
    </section>
    <footer></footer>
</body>
</html>