<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$ubicacion = isset($_GET['ubicacion']) ? $_GET['ubicacion'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino la ubicación
if ($eliminar == "si")
{
    $borrar = $conexion->query("DELETE FROM ubicaciones WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>La ubicación <strong>$ubicacion</strong> fue eliminada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar la ubicación <strong>$ubicacion</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#logistica"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/ubicaciones.jpg');"></div>
            <h2 class="cab_texto">Ubicaciones</h2>
            <div class="bloque_margen">
                <p>Las ubicaciones son los distintos lugares desde los cuales puedes atender a tus clientes en un punto de venta todas las ventas siempre comienzan desde una ubicación, por ejemplo: mesas, barras, habitaciones, cajas registradoras, etc. En esta sección puedes agregar, modificar y eliminar las ubicaciones.</p>
                <p class="alineacion_botonera"><a href="ubicaciones_agregar.php"><input type="button" class="proceder" value="Agregar una nueva ubicación"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Ubicaciones agregadas</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar una ubicación" /></p>                    
                </form>
                <?php
                //consulto el local previamente para la busqueda
                $consulta0 = $conexion->query("SELECT * FROM locales WHERE local like '%$busqueda%'");

                if ($filas0 = $consulta0->fetch_assoc())
                {
                    $local = $filas0['id'];
                }
                else
                {
                    $local = "bullshit";
                }

                //consulto y muestro las ubicaciones
                $consulta = $conexion->query("SELECT * FROM ubicaciones WHERE ubicacion like '%$busqueda%' or ubicada like '%$busqueda%' or estado like '%$busqueda%' or tipo like '%$busqueda%' or local like '%$local%' ORDER BY local, ubicacion");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado ubicaciones para esta búsqueda.</p>

                    <?php
                }
                else
                {
                    ?>

                    <p>Toca una ubicación para verla o editarla.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $ubicacion = $fila['ubicacion'];
                        $ubicada = $fila['ubicada'];
                        $estado = $fila['estado'];
                        $tipo = $fila['tipo'];
                        $local = $fila['local'];

                        //consulto el local
                        $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $local");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $local = $filas2['local'];
                            $local_tipo = $filas2['tipo'];
                        }
                        else
                        {
                            $local = "No se ha asignado un local";
                            $local_tipo = "--";
                        }                      
                        ?>
                        <a href="ubicaciones_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/<?php echo "$tipo"; ?>_<?php echo "$estado"; ?>.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$ubicacion"); ?></span>
                                        <span class="item_descripcion">Ubicada en: <?php echo ucfirst("$ubicada"); ?></span>
                                        <span class="item_descripcion">Local: <?php echo ucfirst("$local"); ?> (<?php echo ucfirst("$local_tipo"); ?>)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>  

    </section>
    <footer></footer>
</body>
</html>