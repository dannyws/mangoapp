<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$eliminar_composicion = isset($_GET['eliminar_composicion']) ? $_GET['eliminar_composicion'] : null ;


$producto_id = isset($_POST['producto_id']) ? $_POST['producto_id'] : null ;
$componente_id = isset($_POST['componente_id']) ? $_POST['componente_id'] : null ;
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : null ;

$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$composicion_id = isset($_GET['composicion_id']) ? $_GET['composicion_id'] : null ;
$componente = isset($_GET['componente']) ? $_GET['componente'] : null ;
?>

<?php
//elimino la ubicación
if ($eliminar_composicion == "si")
{
    $borrar = $conexion->query("DELETE FROM composiciones WHERE id = '$composicion_id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El componente <strong>$componente</strong> fue eliminado exitosamente de la composición de este producto.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el componente <strong>$componente</strong>.</p>";
    }
}
?>

<?php
//agrego el componente a la composición
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM composiciones WHERE producto = '$producto_id' and componente = '$componente_id'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO composiciones values ('', '$ahora', '$sesion_id', '$producto_id', '$componente_id', '$cantidad')");
        
        $mensaje = "<p class='mensaje_exito'>El <strong>componente</strong> fue agregado exitosamente a la composición de este producto.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>Este <strong>componente</strong> ya fue agregado a la composición de este producto, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<?php
//consulto y muestro el producto
$consulta = $conexion->query("SELECT * FROM productos WHERE id = '$id' or id = '$producto_id'");

if ($fila = $consulta->fetch_assoc())
{
    $id = $fila['id'];
    $categoria = $fila['categoria'];
    $producto = $fila['producto'];                   

    //consulto la categoria
    $consulta_categoria = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$categoria'");           

    if ($fila_categoria = $consulta_categoria->fetch_assoc()) 
    {
        $categoria = $fila_categoria['categoria'];
    }
    else
    {
        $categoria = "No se ha asignado una categoría";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="productos_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Producto</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">        

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar componentes a <?php echo ucfirst($producto) ?></h2>
                <?php echo "$mensaje"; ?>
                <p>Seleccione el componente y la cantidad de la que está hecha este producto o servicio. Estos componentes se descontarán automáticamente del inventario local cuando se hagan ventas de productos en la medida en que hayan sido creadas las composiciones.</p>
                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <input type="hidden" name="producto_id" value="<?php echo "$id"; ?>">
                    <p><label for="componente_id">Componente:</label></p>
                    <p><select id="componente_id" name="componente_id" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los componentes
                        $consulta = $conexion->query("SELECT * FROM componentes ORDER BY componente");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_componente = $fila['id'];
                                $componente = $fila['componente'];
                                $unidad = $fila['unidad'];
                                ?>

                                <option value="<?php echo "$id_componente"; ?>"><?php echo ucfirst($componente) ?> (<?php echo ucfirst($unidad) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado componentes</option>

                            <?php
                        }
                        ?>
                    </select></p>



                    <p><label for="cantidad">Cantidad:</label></p>
                    <p><input type="number" id="cantidad" name="cantidad" required /></p>

                    
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Agregar componente</button></p>
                </form>
            </div>
        </article>










        <article class="bloque">
            <div class="bloque_margen">
                <h2>Composición de este producto</h2>
                <p>Toca un componente para eliminarlo de este producto</p>
                <?php                
                //consulto y muestros la composición de este producto
                $consulta = $conexion->query("SELECT * FROM composiciones WHERE producto = '$id' ORDER BY fecha DESC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se ha encontrado composición para este producto.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $composicion_id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $producto_id = $fila['producto'];
                        $componente = $fila['componente'];
                        $cantidad = $fila['cantidad'];

                        //consulto el componente
                        $consulta2 = $conexion->query("SELECT * FROM componentes WHERE id = $componente");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $unidad = $filas2['unidad'];
                            $componente = $filas2['componente'];
                            $precio_unidad = $filas2['precio_unidad'];
                        }
                        else
                        {
                            $componente = "No se ha asignado un componente";
                        }
                        ?>

                        <a href="productos_componentes.php?eliminar_composicion=si&composicion_id=<?php echo ($composicion_id); ?>&componente=<?php echo ($componente); ?>&id=<?php echo ($id); ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$cantidad"); ?> <?php echo ucfirst("$unidad"); ?> de <?php echo ucfirst("$componente"); ?></span>                                    
                                    </div>
                                </div>
                            </div>
                        </a>
                       
                        <?php
                    }
                }
                ?>

            </div>
        </article>

        


    </section>
    <footer></footer>
</body>
</html>