<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$proveedor = isset($_GET['proveedor']) ? $_GET['proveedor'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el proveedor
if ($eliminar == 'si')
{
    $consulta_componentes = $conexion->query("SELECT * FROM componentes WHERE proveedor = '$id'");
    if ($consulta_componentes->num_rows != 0)
    {
        $mensaje = "<p class='mensaje_error'>No es posible eliminar el proveedor <strong>$proveedor</strong> por que aún tiene componentes relacionados a el.</p>";
    }
    else
    {
        $borrar = $conexion->query("DELETE FROM proveedores WHERE id = '$id'");
        if ($borrar)
        {
            $mensaje = "<p class='mensaje_exito'>El proveedor <strong>$proveedor</strong> fue eliminado exitosamente.</p>";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#inventario"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/proveedores.jpg');"></div>
            <h2 class="cab_texto">Proveedores</h2>
            <div class="bloque_margen">
                <p>Los proveedores son las personas o empresas que te venden la materia prima o los ingredientes para la elaboración de tus productos o servicios. En esta sección puedes agregar, modificar o eliminar tus  proveedores.</p>
                <p class="alineacion_botonera"><a href="proveedores_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo proveedor"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Proveedores agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un proveedor" /></p>                    
                </form>
                <?php
                //consulto y muestro los proveedores
                $consulta = $conexion->query("SELECT * FROM proveedores WHERE proveedor like '%$busqueda%' or correo like '%$busqueda%' or telefono like '%$busqueda%' ORDER BY proveedor");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado proveedores para esta búsqueda.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca un proveedor para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $proveedor = $fila['proveedor'];
                        $correo = $fila['correo'];
                        $telefono = $fila['telefono'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/proveedores-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/proveedores-$id-$imagen_nombre-m.jpg";
                        }

                        $consulta_componentes = $conexion->query("SELECT * FROM componentes WHERE proveedor = '$id'");
                        $total_componentes = $consulta_componentes->num_rows;
                        ?>
                        <a href="proveedores_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$proveedor"); ?></span>
                                        <span class="item_descripcion">Correo: <?php echo ("$correo"); ?></span>
                                        <span class="item_descripcion">Teléfono: <?php echo ucfirst("$telefono"); ?></span>
                                        <span class="item_descripcion">Componentes: <?php echo ucfirst("$total_componentes"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>