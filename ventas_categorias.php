<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$ubicacion_id = isset($_GET['ubicacion_id']) ? $_GET['ubicacion_id'] : null ;
$ubicacion = isset($_GET['ubicacion']) ? $_GET['ubicacion'] : null ;
$ubicacion_tipo = isset($_GET['ubicacion_tipo']) ? $_GET['ubicacion_tipo'] : null ;
$venta_id = isset($_GET['venta_id']) ? $_GET['venta_id'] : null ;
$venta_total = isset($_POST['venta_total']) ? $_POST['venta_total'] : null ;
$busqueda = isset($_GET['busqueda']) ? $_GET['busqueda'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
?>

<?php
//escanear codigo de barras
if (isset($busqueda))
{
    $consulta_cod_barras = $conexion->query("SELECT * FROM productos WHERE codigo_barras = '$busqueda'");

    if ($consulta_cod_barras->num_rows != 0)
    {
         while ($fila = $consulta_cod_barras->fetch_assoc())
        {
            $producto_id = $fila['id'];
            $categoria = $fila['categoria'];
            $producto = $fila['producto'];
            $precio_bruto = $fila['precio'];
            $impuesto = $fila['impuesto'];            

            //consulto la categoria
            $consulta_categoria = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$categoria'");

            while ($fila_categoria = $consulta_categoria->fetch_assoc())
            {
                $categoria_id = $fila_categoria['id'];
                $categoria = $fila_categoria['categoria'];
            }

            //consulto el impuesto
            $consulta_impuesto = $conexion->query("SELECT * FROM impuestos WHERE id = '$impuesto'");

            while ($fila_impuesto = $consulta_impuesto->fetch_assoc())
            {
                $impuesto_porcentaje = $fila_impuesto['porcentaje'];
            }

            //precio con el impuesto
            $precio_neto = (($impuesto_porcentaje * $precio_bruto) / 100) + $precio_bruto;
        }

        $insercion = $conexion->query("INSERT INTO ventas_productos values ('', now(), '$sesion_id', '$venta_id', '$ubicacion_id', '$ubicacion', '$categoria_id', '$categoria', '$producto_id', '$producto', '$precio_bruto', '$impuesto_porcentaje', '$precio_neto')");
        $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue agregado exitosamente.</p>";
    }
}
?>

<?php
//consulto si hay una venta en la ubicacion en estado OCUPADO
$consulta = $conexion->query("SELECT * FROM ventas_datos WHERE local_id = '$sesion_local_id' and estado = 'ocupado' and ubicacion_id='$ubicacion_id'");

//si ya existe una venta creada en esa ubicacion en estado OCUPADO consulto el id de la venta
if ($fila = $consulta->fetch_assoc())
{
    $venta_id = $fila['id'];
}
else
{
    //si no la hay guardo los datos iniciales de la venta
    $insercion = $conexion->query("INSERT INTO ventas_datos values ('', '$ahora', '$sesion_id', '$sesion_local_id', '$ubicacion_id', '$ubicacion', 'efectivo', 'ocupado', '0', '0', '0', '0')");

    //consulto el ultimo id que se ingreso para tenerlo como id de la venta
    $venta_id = $conexion->insert_id;
   
    //actualizo el estado de la ubicación a OCUPADO
    $actualizar = $conexion->query("UPDATE ubicaciones SET estado = 'ocupado' WHERE ubicacion = '$ubicacion' and local = '$sesion_local_id'");
}
?>

<?php
//consulto el total de los productos ingresados a la venta
$consulta_venta_total = $conexion->query("SELECT * FROM ventas_productos WHERE venta_id = '$venta_id'");

while ($fila_venta_total = $consulta_venta_total->fetch_assoc())
{
    $precio = $fila_venta_total['precio_final'];

    $venta_total = $venta_total + $precio;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="ventas_ubicaciones.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div><span class="logo_txt"> Ubicaciones</span></h2>
                </div>
            </a>
            <a href="ventas_ubicaciones.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ventas_resumen.php?venta_id=<?php echo "$venta_id";?>">
                <div class="cabezote_col_der">
                    <h2><span class="logo_txt">$ <?php echo number_format($venta_total, 0, ",", "."); ?></span> <div class="flecha_der"></div></h2>
                </div>
            </a>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">
                <h2><span class="descripcion"><?php echo ucfirst($ubicacion)?> / </span>Categorías</h2>

                <?php
                //si la ubicaion es CAJA muestro el campo para escanear codigos de barras
                if ($ubicacion_tipo == "caja") 
                {
                ?>

                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" enctype="multipart/form-data">
                    <input type="hidden" name="venta_id" value="<?php echo "$venta_id";?>" />
                    <input type="hidden" name="ubicacion_id" value="<?php echo "$ubicacion_id";?>" />
                    <input type="hidden" name="ubicacion" value="<?php echo "$ubicacion";?>" />
                    <p><input type="text" name="busqueda" value="" placeholder="Escanear código de barras" autofocus /></p>
                </form>

                <?php
                }
                ?>

                <?php
                //consulto y muestro las categorías
                $consulta = $conexion->query("SELECT * FROM productos_categorias ORDER BY categoria");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han agregado categorías.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una categoría para ver sus productos.</p>
                    <?php echo "$mensaje";?>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $categoria_id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $categoria = $fila['categoria'];
                        $tipo = $fila['tipo'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/categorias-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/categorias-$categoria_id-$imagen_nombre-m.jpg";
                        }

                        //consulto la cantidad de productos de esa categoria
                        $consulta_productos = $conexion->query("SELECT * FROM productos WHERE categoria = '$categoria_id' and (local = '$sesion_local_id' or local = '0') ORDER BY producto");
                        $registros_productos = $consulta_productos->num_rows;

                        if ($registros_productos != 0)
                        {
                            ?>

                            <a href="ventas_productos.php?categoria_id=<?php echo "$categoria_id"; ?>&categoria=<?php echo "$categoria";?>&venta_id=<?php echo "$venta_id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen"; ?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$categoria"); ?></span>
                                        <span class="item_descripcion"><?php echo "$registros_productos";; ?> <?php echo "$tipo";?></span>
                                    </div>
                                </div>
                            </div>
                            </a>
                            
                            <?php
                        }
                        ?>                        
                        
                        <?php
                    }
                }
                ?>                
            </div>
        </article>

    </section>

    <footer></footer>

</body>
</html>