<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$ubicacion = isset($_POST['ubicacion']) ? $_POST['ubicacion'] : null ;
$ubicada = isset($_POST['ubicada']) ? $_POST['ubicada'] : null ;
$estado = isset($_POST['estado']) ? $_POST['estado'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
?>

<?php
//agregar la ubicacion
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM ubicaciones WHERE ubicacion = '$ubicacion' and ubicada = '$ubicada' and tipo = '$tipo' and local = '$local'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO ubicaciones values ('', '$ahora', '$sesion_id', '$ubicacion', '$ubicada', 'libre', '$tipo', '$local')");
        $mensaje = "<p class='mensaje_exito'>La ubicacion <strong>$ubicacion</strong> fue agregada exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>La ubicación <strong>$ubicacion</strong> ya existe, no es posible agregarla de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ubicaciones_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Ubicaciones</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar una nueva ubicación</h2>
                <?php echo "$mensaje"; ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <p><label for="ubicacion">Ubicación:</label></p>
                    <p><input type="text" id="ubicacion" name="ubicacion" required autofocus /></p>
                    <p><label for="ubicada">Ubicada en:</label></p>
                    <p><input type="text" id="ubicada" name="ubicada" required /></p>                    
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value=""></option>
                        <option value="caja">Caja</option>
                        <option value="barra">Barra</option>
                        <option value="habitacion">Habitación</option>
                        <option value="mesa">Mesa</option>
                    </select></p>
                    <p><label for="local">Local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>