<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;
$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$tipo_pago = isset($_POST['tipo_pago']) ? $_POST['tipo_pago'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
?>

<?php
//actualizo la información del tipo de pago
if ($editar == "si")
{
    $actualizar = $conexion->query("UPDATE tipos_pagos SET fecha = '$ahora', usuario = '$sesion_id', tipo_pago = '$tipo_pago', tipo = '$tipo' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El tipo de pago <strong>$tipo_pago</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el tipo de pago.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="tipos_pagos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> T. de pago</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            
            <?php
            //consulto y muestro el tipo de pago
            $consulta = $conexion->query("SELECT * FROM tipos_pagos WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Tipo de pago eliminado</h2>
                    <p class="mensaje_error">Esta tipo de pago ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $tipo_pago = $fila['tipo_pago'];
                    $tipo = $fila['tipo'];

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('img/iconos/<?php echo "$tipo" ?>.jpg');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$tipo_pago"); ?></h2>
                    <div class="bloque_margen">                        
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Tipo</span><?php echo ucfirst($tipo) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="tipos_pagos_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este tipo de pago"></a></p>
                    </div>
                    <?php
                }
            }
            ?>  
            
        </article>
    </section>
    <footer></footer>
</body>
</html>