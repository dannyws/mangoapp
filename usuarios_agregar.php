<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$correo = isset($_POST['correo']) ? $_POST['correo'] : null ;
$contrasena = isset($_POST['contrasena']) ? $_POST['contrasena'] : null ;
$nombres = isset($_POST['nombres']) ? $_POST['nombres'] : null ;
$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
?>

<?php
//agregar el usuario
if ($agregar == 'si')
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
    }
    else
    {
        $imagen = "no";
    }

    $consulta = $conexion->query("SELECT correo FROM usuarios WHERE correo = '$correo'");

    if ($consulta->num_rows == 0)
    {
        $imagen_ref = "usuarios";
        $insercion = $conexion->query("INSERT INTO usuarios values ('', '$ahora', '$sesion_id', '$correo', '$contrasena', '$nombres', '$apellidos', '$tipo', '$local', '$imagen', '$ahora_img')");
        $mensaje = "<p class='mensaje_exito'>El usuario <strong>$correo</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;        

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El usuario <strong>$correo</strong> ya existe, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="usuarios_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Usuarios</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo usuario</h2>
                <?php echo "$mensaje"; ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <p><label for="correo">Correo:</label></p>
                    <p><input type="email" id="correo" name="correo" required autofocus /></p>
                    <p><label for="contrasena">Contraseña:</label></p>
                    <p><input type="text" id="contrasena" name="contrasena" required /></p>
                    <p><label for="nombres">Nombres:</label></p>
                    <p><input type="text" id="nombres" name="nombres" required /></p>
                    <p><label for="apellidos">Apellidos:</label></p>
                    <p><input type="text" id="apellidos" name="apellidos" required /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value=""></option>
                        <option value="administrador">Administrador</option>
                        <option value="mesero">Mesero</option>
                        <option value="socio">Socio</option>
                        <option value="vendedor">Vendedor</option>
                    </select></p>
                    <p><label for="local">Local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body> 
</html>