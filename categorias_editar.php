<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información de la categoría
$consulta = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $categoria = $fila['categoria'];
    $tipo = $fila['tipo'];
    $imagen = $fila['imagen'];
    $imagen_nombre = $fila['imagen_nombre'];
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="categorias_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Categoría</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar esta categoría</h2>
                <form action="categorias_detalle.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>" />
                    <input type="hidden" name="imagen" value="<?php echo "$imagen"; ?>" />
                    <input type="hidden" name="imagen_nombre" value="<?php echo "$imagen_nombre"; ?>" />
                    <p><label for="categoria">Categoría:</label></p>
                    <p><input type="text" id="categoria" name="categoria" value="<?php echo "$categoria"; ?>" required autofocus /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst("$tipo"); ?></option>
                        <option value=""></option>
                        <option value="productos">Productos</option>
                        <option value="servicios">Servicios</option>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="categorias_ver.php?eliminar=si&id=<?php echo "$id"; ?>&categoria=<?php echo "$categoria"; ?>"><input type="button" class="advertencia" value="Eliminar esta categoría"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>