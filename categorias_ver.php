<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$categoria = isset($_GET['categoria']) ? $_GET['categoria'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino la categoria
if ($eliminar == 'si')
{
    $consulta_productos = $conexion->query("SELECT * FROM productos WHERE categoria = '$id'");
    if ($consulta_productos->num_rows != 0)
    {
        $mensaje = "<p class='mensaje_error'>No es posible eliminar la categoría <strong>$categoria</strong> por que aún tiene productos relacionados a ella.</p>";
    }
    else
    {
        $borrar = $conexion->query("DELETE FROM productos_categorias WHERE id = '$id'");
        if ($borrar)
        {
            $mensaje = "<p class='mensaje_exito'>La categoría <strong>$categoria</strong> fue eliminada exitosamente.</p>";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#portafolio"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/categorias.jpg');"></div>
            <h2 class="cab_texto">Categorias</h2>
            <div class="bloque_margen">
                <p>Las categorías son los grupos o familias en los que están divididos los productos o servicios que vendes en tu negocio, por ejemplo: carnes, bebidas, licores, postres, etc. En esta sección puedes agregar, modificar y eliminar las categorías.</p>
                <p class="alineacion_botonera"><a href="categorias_agregar.php"><input type="button" class="proceder" value="Agregar una nueva categoría"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Categorías agregadas</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar una categoría" /></p>                    
                </form>
                <?php
                //consulto y muestro las categorías
                $consulta = $conexion->query("SELECT * FROM productos_categorias WHERE categoria like '%$busqueda%' or tipo like '%$busqueda%' ORDER BY categoria");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado categorías para esta búsqueda.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una categoría para verla o editarla.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $categoria = $fila['categoria'];
                        $tipo = $fila['tipo'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/categorias-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/categorias-$id-$imagen_nombre-m.jpg";
                        }

                        $consulta_productos = $conexion->query("SELECT * FROM productos WHERE categoria = '$id'");
                        $total_productos = $consulta_productos->num_rows;
                        ?>
                        <a href="categorias_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$categoria"); ?></span>
                                        <span class="item_descripcion">Productos: <?php echo ucfirst("$total_productos"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>