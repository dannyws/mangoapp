<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del tipo de pago
$consulta = $conexion->query("SELECT * FROM tipos_pagos WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $tipo_pago = $fila['tipo_pago'];
    $tipo = $fila['tipo'];
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="tipos_pagos_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Tipo de pago</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este tipo de pago</h2>
                <form action="tipos_pagos_detalle.php" method="post">
                <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                <p><label for="tipo_pago">Tipo de pago:</label></p>
                <p><input type="text" id="tipo_pago" name="tipo_pago" value="<?php echo "$tipo_pago"; ?>" required autofocus /></p>                
                <p><label for="tipo">Tipo:</label></p>
                <p><select id="tipo" name="tipo" required>
                    <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst($tipo) ?></option>
                    <option value=""></option>
                    <option value="canje">Canje</option>
                    <option value="cheque">Cheque</option>
                    <option value="efectivo">Efectivo</option>
                    <option value="tarjeta">Tarjeta</option>
                </select></p>               
                <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="tipos_pagos_ver.php?eliminar=si&id=<?php echo "$id"; ?>&tipo_pago=<?php echo "$tipo_pago"; ?>"><input type="button" class="advertencia" value="Eliminar este tipo de pago"></a></p>
            </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>