<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$local = isset($_GET['local']) ? $_GET['local'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el local
if ($eliminar == 'si')
{
    $consulta_usuarios = $conexion->query("SELECT * FROM usuarios WHERE local = '$id'");
    if ($consulta_usuarios->num_rows == 0)
    {
        $consulta_ubicaciones = $conexion->query("SELECT * FROM ubicaciones WHERE local = '$id'");
        if ($consulta_ubicaciones->num_rows == 0)
        {
            $borrar = $conexion->query("DELETE FROM locales WHERE id = '$id'");

            if ($borrar)
            {
                $mensaje = "<p class='mensaje_exito'>El local <strong>$local</strong> fue eliminado exitosamente.</p>";
            }
        }
        else
        {
            $mensaje = "<p class='mensaje_error'>No es posible eliminar el local <strong>$local</strong> por que aún tiene ubicaciones relacionadas a él.</p>";
        }
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No es posible eliminar el local <strong>$local</strong> por que aún tiene usuarios relacionados a él.</p>";

    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#logistica"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/locales.jpg');"></div>
            <h2 class="cab_texto">Locales</h2>
            <div class="bloque_margen">
                <p>Los locales son todos los puntos de venta que puedes tener en tu negocio, por ejemplo: punto de venta Bogotá, punto de venta Medellín, punto de venta barrio poblado, punto de venta centro comercial del norte, etc. En esta sección puedes agregar, modificar y eliminar los locales.</p>
                <?php echo "$mensaje"; ?>
                <p class="alineacion_botonera"><a href="locales_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo local"></a></p>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Locales agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un local" /></p>                    
                </form>
                <?php
                //consulto los locales                
                $consulta = $conexion->query("SELECT * FROM locales WHERE local like '%$busqueda%' or direccion like '%$busqueda%' or telefono like '%$busqueda%' or tipo like '%$busqueda%' ORDER BY local");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado locales para esta búsqueda.</p>

                    <?php
                }

                else
                {   ?>

                    <p>Toca un local para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc()) 
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $local = $fila['local'];
                        $direccion = $fila['direccion'];
                        $telefono = $fila['telefono'];
                        $tipo = $fila['tipo'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/locales-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/locales-$id-$imagen_nombre-m.jpg";
                        }

                        $consulta_usuarios = $conexion->query("SELECT * FROM usuarios WHERE local = '$id'");
                        $total_usuarios = $consulta_usuarios->num_rows;

                        $consulta_ubicaciones = $conexion->query("SELECT * FROM ubicaciones WHERE local = '$id'");
                        $total_ubicaciones = $consulta_ubicaciones->num_rows;
                        ?>
                        <a href="locales_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img_top">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$local"); ?></span>
                                        <span class="item_descripcion">Direccion: <?php echo ucfirst("$direccion"); ?></span>
                                        <span class="item_descripcion">Tipo: <?php echo ucfirst("$tipo"); ?></span>
                                        <span class="item_descripcion">Usuarios: <?php echo ucfirst("$total_usuarios"); ?></span>
                                        <span class="item_descripcion">Ubicaciones: <?php echo ucfirst("$total_ubicaciones"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>