<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$tipo_pago = isset($_GET['tipo_pago']) ? $_GET['tipo_pago'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el tipo de pago
if ($eliminar == 'si')
{
    $borrar = $conexion->query("DELETE FROM tipos_pagos WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El tipo de pago <strong>$tipo_pago</strong> fue eliminado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el tipo de pago <strong>$tipo_pago</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#financiero"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/tipos_pago.jpg');"></div>
            <h2 class="cab_texto">Tipos de pago</h2>
            <div class="bloque_margen">
                <p>Los tipos de pagos son los diferentes medios de cambio que recibes en tu negocio cuando tus clientes hacen una compra, por ejemplo: efectivo, tarjeta débito, cheque, canje de servicios, etc. En esta sección puedes agregar, modificar y eliminar los tipos de pago.</p>
                <p class="alineacion_botonera"><a href="tipos_pagos_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo tipo de pago"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Tipos de pago agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un tipo de pago" /></p>                    
                </form>
                <?php
                //consulto y muestro los tipos de pago
                $consulta = $conexion->query("SELECT * FROM tipos_pagos WHERE tipo_pago like '%$busqueda%' or tipo like '%$busqueda%' ORDER BY tipo_pago");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado tipos de pago para esta búsqueda.</p>

                    <?php
                } 


                else                 
                {
                    ?>

                    <p>Toca un tipo de pago para verlo o editarlo.</p>

                    <?php
                     while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $tipo_pago = $fila['tipo_pago'];
                        $tipo = $fila['tipo'];
                        ?>
                        <a href="tipos_pagos_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('img/iconos/<?php echo "$tipo"; ?>.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$tipo_pago"); ?></span>
                                        <span class="item_descripcion">Tipo: <?php echo ucfirst("$tipo"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }                    
                }
                ?>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>