<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información de la plantilla de factura
$consulta = $conexion->query("SELECT * FROM facturas_plantillas WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $nombre = $fila['nombre'];
    $titulo = $fila['titulo'];
    $texto_superior = $fila['texto_superior'];
    $texto_inferior = $fila['texto_inferior'];
    $local = $fila['local'];

    //consulto el local
    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

    if ($fila = $consulta_local->fetch_assoc()) 
    {
        $local_id = $fila['id'];
        $local = $fila['local'];
        $local_tipo = $fila['tipo'];
    }
    else
    {
        $local_id = "0";
        $local = "Todos los locales";
        $local_tipo = "Todos";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="facturas_plantillas_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> P. de factura</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar esta plantilla de factura</h2>
                <form action="facturas_plantillas_detalle.php" method="post">
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                    <p><label for="nombre">Nombre de la plantilla:</label></p>
                    <p><input type="text" id="nombre" name="nombre" value="<?php echo "$nombre"; ?>" required autofocus /></p>
                    <p><label for="titulo">Titulo de la factura:</label></p>
                    <p><input type="text" id="titulo" name="titulo" value="<?php echo "$titulo"; ?>" required /></p>
                    <p><label for="texto_superior">Texto superior:</label></p>
                    <p><textarea id="texto_superior" name="texto_superior"><?php echo ($texto_superior) ?></textarea></p>
                    <p><label for="texto_inferior">Texto inferior:</label></p>
                    <p><textarea id="texto_inferior" name="texto_inferior"><?php echo ($texto_inferior) ?></textarea></p>


                    
                    <p><label for="local">Local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value="<?php echo "$local_id"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($local_tipo) ?>)</option>
                        <option value=""></option>
                        <option value="0">Todos los locales</option>
                    <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="facturas_plantillas_ver.php?eliminar=si&id=<?php echo "$id"; ?>&nombre=<?php echo "$nombre"; ?>"><input type="button" class="advertencia" value="Eliminar esta plantilla de factura"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>