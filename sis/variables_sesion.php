<?php 
//consulto las variables de la sesion
$sesion_id = $_SESSION['id'];

$conexion = new mysqli($conexion_host, $conexion_user, $conexion_pass, $conexion_bd);
$consulta = $conexion->query("SELECT * FROM usuarios WHERE id = $sesion_id");

while ($fila = $consulta->fetch_assoc()) 
{
    $sesion_id = $fila['id'];
    $sesion_nombres = $fila['nombres'];
    $sesion_apellidos = $fila['apellidos'];
    $sesion_correo = $fila['correo'];
    $sesion_tipo = $fila['tipo'];
    $sesion_local_id = $fila['local'];
    $sesion_imagen = $fila['imagen'];
    $sesion_imagen_nombre = $fila['imagen_nombre'];

    if ($sesion_imagen == "no")
    {
        $sesion_imagen = "img/iconos/usuarios.jpg";
    }
    else
    {
        $sesion_imagen = "img/avatares/usuarios-$sesion_id-$sesion_imagen_nombre.jpg";
    }    

    $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $sesion_local_id");

    if ($fila = $consulta2->fetch_assoc())
    {
        $sesion_local = $fila['local'];
        $sesion_local_tipo = $fila['tipo'];
        $sesion_local_direccion = $fila['direccion'];
        $sesion_local_telefono = $fila['telefono'];
    }
}
?>

<?php
//time zone
date_default_timezone_set('America/Bogota');
$ahora = date("Y-m-d H:i:s");
$ahora_img = date("YmdHis");
?>