<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$guardar_producto = isset($_GET['guardar_producto']) ? $_GET['guardar_producto'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$ubicacion_id = isset($_GET['ubicacion_id']) ? $_GET['ubicacion_id'] : null ;
$ubicacion = isset($_GET['ubicacion']) ? $_GET['ubicacion'] : null ;
$zona = isset($_GET['zona']) ? $_GET['zona'] : null ;
$categoria_id = isset($_GET['categoria_id']) ? $_GET['categoria_id'] : null ;
$categoria = isset($_GET['categoria']) ? $_GET['categoria'] : null ;
$venta_id = isset($_GET['venta_id']) ? $_GET['venta_id'] : null ;

$producto_id = isset($_GET['producto_id']) ? $_GET['producto_id'] : null ;
$producto = isset($_GET['producto']) ? $_GET['producto'] : null ;
$precio_final = isset($_GET['precio_final']) ? $_GET['precio_final'] : null ;
$porcentaje_impuesto = isset($_GET['porcentaje_impuesto']) ? $_GET['porcentaje_impuesto'] : null ;
?>

<?php
//consulto los datos de la venta
$consulta_venta = $conexion->query("SELECT * FROM ventas_datos WHERE id = '$venta_id' and estado = 'ocupado'");

if ($consulta_venta->num_rows == 0)
{
    header("location:ventas_ubicaciones.php");
}
else
{
    while ($fila_venta = $consulta_venta->fetch_assoc())
    {
        $ubicacion_id = $fila_venta['ubicacion_id'];
        $ubicacion = $fila_venta['ubicacion'];
    }
}
?>

<?php
//guardo el producto de la venta
if ($guardar_producto == "si")
{
    $insercion = $conexion->query("INSERT INTO ventas_productos values ('', '$ahora', '$sesion_id', '$venta_id', '$ubicacion_id', '$ubicacion', '$categoria_id', '$categoria', '$sesion_local_id', '$zona','$producto_id', '$producto', '$precio_final', '$porcentaje_impuesto', 'pedido')");
    $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue agregado exitosamente.</p>";
}
?>

<?php
//consulto el total de los productos ingresados a la venta
$consulta_venta_total = $conexion->query("SELECT * FROM ventas_productos WHERE venta_id = '$venta_id'");

$venta_total = 0;

while ($fila_venta_total = $consulta_venta_total->fetch_assoc())
{
    $precio = $fila_venta_total['precio_final'];

    $venta_total = $venta_total + $precio;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="ventas_categorias.php?ubicacion_id=<?php echo "$ubicacion_id";?>&ubicacion=<?php echo "$ubicacion";?>">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt"> Categorías</span></h2>
                </div>
            </a>
            <a href="ventas_ubicaciones.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ventas_resumen.php?venta_id=<?php echo "$venta_id";?>">
                <div class="cabezote_col_der">
                    <h2><span class="logo_txt">$ <?php echo number_format($venta_total, 0, ",", "."); ?></span> <div class="flecha_der"></div></h2>
                </div>
            </a>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">

                <h2><span class="descripcion"><?php echo ucfirst($ubicacion) ;?> / <?php echo ucfirst($categoria) ;?> / </span>Productos</h2>
                <?php
                //consulto y muestro los productos de la categoria seleccionada en el local permitido
                $consulta = $conexion->query("SELECT * FROM productos WHERE categoria = '$categoria_id' and (local = '$sesion_local_id' or local = '0') ORDER BY producto");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han agregado productos a esta categoría o a este local.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una producto para agregarlo a la venta.</p>
                    <?//php echo "$mensaje";?>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $producto_id = $fila['id'];
                        $categoria = $fila['categoria'];
                        $zona = $fila['zona'];
                        $producto = $fila['producto'];
                        $precio_final = $fila['precio'];
                        $impuesto = $fila['impuesto'];
                        $descripcion = $fila['descripcion'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/productos-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/productos-$producto_id-$imagen_nombre-m.jpg";
                        }

                        //cantidad de productos en este venta                        
                        $consulta_cantidad = $conexion->query("SELECT * FROM ventas_productos WHERE producto_id = '$producto_id' and venta_id = '$venta_id'");

                        if ($consulta_cantidad->num_rows == 0)
                        {
                            $cantidad = "";
                        }
                        else
                        {
                            $cantidad = $consulta_cantidad->num_rows;
                            $cantidad = "<span class='texto_exito'>x $cantidad</span>";
                        }                                                

                        //consulto la categoria
                        $consulta_categoria = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$categoria'");

                        while ($fila_categoria = $consulta_categoria->fetch_assoc())
                        {
                            $categoria_id = $fila_categoria['id'];
                            $categoria = $fila_categoria['categoria'];
                        }

                        //consulto el impuesto
                        $consulta_impuesto = $conexion->query("SELECT * FROM impuestos WHERE id = '$impuesto'");

                        while ($fila_impuesto = $consulta_impuesto->fetch_assoc())
                        {
                            $porcentaje_impuesto = $fila_impuesto['porcentaje'];
                        }

                        ?>
                        <a href="ventas_productos.php?guardar_producto=si&venta_id=<?php echo "$venta_id";?>&ubicacion_id=<?php echo "$ubicacion_id";?>&ubicacion=<?php echo "$ubicacion";?>&categoria_id=<?php echo "$categoria_id";?>&categoria=<?php echo "$categoria";?>&zona=<?php echo "$zona";?>&producto_id=<?php echo "$producto_id";?>&producto=<?php echo "$producto";?>&precio_final=<?php echo "$precio_final";?>&porcentaje_impuesto=<?php echo "$porcentaje_impuesto";?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img_top">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen"; ?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$producto"); ?> <?php echo ucfirst("$cantidad"); ?></span>
                                        <span class="item_descripcion_claro"><em><?php echo ucfirst("$descripcion"); ?></em></span>
                                        <span class="item_descripcion">$ <?php echo number_format($precio_final, 0, ",", "."); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>

        </article>

    </section>

    <footer></footer>

</body>
</html>