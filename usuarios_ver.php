<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$correo = isset($_GET['correo']) ? $_GET['correo'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el usuario
if ($eliminar == 'si')
{
    $borrar = $conexion->query("DELETE FROM usuarios WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El usuario <strong>$correo</strong> fue eliminado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el usuario <strong>$correo</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#logistica"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/usuarios.jpg');"></div>
            <h2 class="cab_texto">Usuarios</h2>
            <div class="bloque_margen">
                <p>Los usuarios son todas las personas que interactúan en tu negocio, por ejemplo: meseros, vendedores, socios, administradores, etc. En esta sección puedes agregar, modificar y eliminar los usuarios.</p>
                <?php echo "$mensaje"; ?>
                <p class="alineacion_botonera"><a href="usuarios_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo usuario"></a></p>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Usuarios agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un usuario" /></p>                    
                </form>
                <?php
                //consulto el local previamente para la busqueda
                $consulta0 = $conexion->query("SELECT * FROM locales WHERE local like '%$busqueda%'");

                if ($filas0 = $consulta0->fetch_assoc())
                {
                    $local = $filas0['id'];
                }
                else
                {
                    $local = "bullshit";
                }

                //consulto los usuarios
                $consulta = $conexion->query("SELECT * FROM usuarios WHERE correo like '%$busqueda%' or nombres like '%$busqueda%' or apellidos like '%$busqueda%' or tipo like '%$busqueda%' or local like '%$local%' ORDER BY nombres");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado usuarios para esta búsqueda.</p>

                    <?php
                }
                else
                {   ?>

                    <p>Toca un usuario para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $correo = $fila['correo'];
                        $contrasena = $fila['contrasena'];
                        $nombres = $fila['nombres'];
                        $apellidos = $fila['apellidos'];
                        $tipo = $fila['tipo'];
                        $local = $fila['local'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/usuarios-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/usuarios-$id-$imagen_nombre-m.jpg";
                        }

                        //consulto el local
                        $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $local");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $local = $filas2['local'];
                            $local_tipo = $filas2['tipo'];
                        }
                        else
                        {
                            $local = "No se ha asignado un local";
                            $local_tipo = "--";
                        }
                        ?>
                        <a href="usuarios_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucwords("$nombres"); ?> <?php echo ucwords("$apellidos"); ?></span>
                                        <span class="item_descripcion">Tipo: <?php echo ucfirst("$tipo"); ?></span>
                                        <span class="item_descripcion">Local: <?php echo ucfirst("$local"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>
        
    </section>
    <footer></footer>
</body>
</html>