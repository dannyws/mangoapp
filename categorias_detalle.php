<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;

$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : null ;
$imagen_nombre = isset($_POST['imagen_nombre']) ? $_POST['imagen_nombre'] : null ;
$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//actualizo la información de la categoría
if ($editar == "si")
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
        $imagen_nombre = $ahora_img;
        $imagen_ref = "categorias";

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $imagen = $imagen;
        $imagen_nombre = $imagen_nombre;
    }

    $actualizar = $conexion->query("UPDATE productos_categorias SET fecha = '$ahora', usuario = '$sesion_id', categoria = '$categoria', tipo = '$tipo', imagen = '$imagen', imagen_nombre = '$imagen_nombre' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>La categoría <strong>$categoria</strong> fue modificada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar la categoría.";
    }        
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="categorias_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Categorías</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <?php
            //consulto y muestro la categoría
            $consulta = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Categoría eliminada</h2>
                    <p class="mensaje_error">Esta categoría ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $categoria = $fila['categoria'];
                    $tipo = $fila['tipo'];
                    $imagen = $fila['imagen'];
                    $imagen_nombre = $fila['imagen_nombre'];

                    if ($imagen == "no")
                    {
                        $imagen = "img/iconos/categorias.jpg";
                    }
                    else
                    {
                        $imagen = "img/avatares/categorias-$id-$imagen_nombre.jpg";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('<?php echo ($imagen) ?>');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$categoria"); ?></h2>
                    <div class="bloque_margen">                        
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Tipo</span><?php echo ucfirst($tipo) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="categorias_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar esta categoría"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Productos relacionados a esta categoría</h2>                
                <?php
                //consulto los productos
                $consulta = $conexion->query("SELECT * FROM productos WHERE categoria = '$id' ORDER BY producto");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado <strong>productos</strong> relacionados a esta categoría.</p>

                    <?php
                }
                else
                {   
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $categoria = $fila['categoria'];
                        $producto = $fila['producto'];
                        $precio = $fila['precio'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/productos-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/productos-$id-$imagen_nombre-m.jpg";
                        }
                        ?>
                                                    
                        <div class="item">
                            <div class="item">
                                <div class="item_img">                            
                                    <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                </div>
                                <div class="item_info">
                                    <span class="item_titulo"><?php echo ucfirst("$producto"); ?></span>
                                    <span class="item_descripcion">$ <?php echo number_format($precio, 0, ",", "."); ?></span>
                                </div>
                            </div>
                        </div>
                       
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>