<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$venta_id = isset($_GET['venta_id']) ? $_GET['venta_id'] : null ;
$producto_venta_id = isset($_GET['producto_venta_id']) ? $_GET['producto_venta_id'] : null ;
$producto = isset($_GET['producto']) ? $_GET['producto'] : null ;
$venta_total = isset($_POST['venta_total']) ? $_POST['venta_total'] : null ;
?>

<?php
//elimino el producto
if ($eliminar == 'si')
{
    $borrar = $conexion->query("DELETE FROM ventas_productos WHERE id = $producto_venta_id");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue eliminado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el producto <strong>$producto</strong>.</p>";
    }
}
?>

<?php
//consulto los datos de la venta
$consulta_venta = $conexion->query("SELECT * FROM ventas_datos WHERE id = '$venta_id' and estado = 'ocupado'");    

if ($consulta_venta->num_rows == 0)
{
    header("location:ventas_ubicaciones.php");
}
else
{
    while ($fila_venta = $consulta_venta->fetch_assoc())
    {
        $ubicacion_id = $fila_venta['ubicacion_id'];
        $ubicacion = $fila_venta['ubicacion'];
    }
}
?>

<?php
//consulto el total de los productos ingresados a la venta
$consulta_venta_total = $conexion->query("SELECT * FROM ventas_productos WHERE venta_id = '$venta_id'");

while ($fila_venta_total = $consulta_venta_total->fetch_assoc())
{
    $precio = $fila_venta_total['precio_final'];

    $venta_total = $venta_total + $precio;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="ventas_categorias.php?venta_id=<?php echo "$venta_id";?>&ubicacion_id=<?php echo "$ubicacion_id";?>&ubicacion=<?php echo "$ubicacion";?>">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt"> Categorías</span></h2>
                </div>
            </a>
            <a href="ventas_ubicaciones.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ventas_descuentos.php?venta_id=<?php echo "$venta_id";?>">
                <div class="cabezote_col_der">
                    <h2><span class="logo_txt">$ <?php echo number_format($venta_total, 0, ",", "."); ?></span> <div class="flecha_der"></div></h2>
                </div>
            </a>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">

                <h2><span class="descripcion"><?php echo ucfirst($ubicacion) ;?> / </span>Resúmen</h2>
                <?php
                //consulto y muestro los productos agregados a la venta
                $consulta = $conexion->query("SELECT distinct producto FROM ventas_productos WHERE venta_id = '$venta_id' ORDER BY fecha DESC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han agregado productos a esta venta.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una producto para eliminarlo de la venta.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $producto = $fila['producto'];

                        //consulto la información del producto
                        $consulta_producto = $conexion->query("SELECT * FROM ventas_productos WHERE producto = '$producto' and venta_id = '$venta_id' ORDER BY fecha DESC");

                        while ($fila_producto = $consulta_producto->fetch_assoc())
                        {
                            $producto_venta_id = $fila_producto['id'];
                            $fecha = date('d M', strtotime($fila_producto['fecha']));                       
                            $categoria = $fila_producto['categoria'];
                            $producto_id = $fila_producto['producto_id'];
                            $producto = $fila_producto['producto'];
                            $precio_final = $fila_producto['precio_final'];

                            //cantidad de productos en este venta                        
                            $consulta_cantidad = $conexion->query("SELECT * FROM ventas_productos WHERE producto_id = '$producto_id' and venta_id = '$venta_id'");

                            if ($consulta_cantidad->num_rows == 0)
                            {
                                $cantidad = "";
                            }
                            else
                            {
                                $cantidad = $consulta_cantidad->num_rows;
                                $cantidad = "<span class='texto_exito'>x $cantidad</span>";
                            }

                            //consulto la imagen del producto
                            $consulta_producto_img = $conexion->query("SELECT * FROM productos WHERE id = '$producto_id'");                        

                            while ($fila_producto_img = $consulta_producto_img->fetch_assoc())
                            {
                                $imagen = $fila_producto_img['imagen'];
                                $imagen_nombre = $fila_producto_img['imagen_nombre'];

                                if ($imagen == "no")
                                {
                                    $imagen = "img/iconos/productos-m.jpg";
                                }
                                else
                                {
                                    $imagen = "img/avatares/productos-$producto_id-$imagen_nombre-m.jpg";
                                }
                            }                            
                        }                        
                        ?>

                        <a href="ventas_resumen.php?eliminar=si&producto_venta_id=<?php echo "$producto_venta_id";?>&producto=<?php echo "$producto";?>&venta_id=<?php echo "$venta_id";?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen"; ?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$producto"); ?> <?php echo ucfirst("$cantidad"); ?></span>
                                        <span class="item_descripcion_claro"><em><?php echo ucfirst("$categoria"); ?></em></span>
                                        <span class="item_descripcion">$ <?php echo number_format($precio_final, 0, ",", "."); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <?php
                    }
                }
                ?>

                <p class="alineacion_botonera"><a href="ventas_ubicaciones.php?eliminar_venta=si&venta_id=<?php echo "$venta_id";?>&ubicacion_id=<?php echo "$ubicacion_id";?>"><input type="button" class="advertencia" value="Eliminar esta venta y liberar ubicación"></a></p>

            </div>

        </article>

    </section>

    <footer></footer>

</body>
</html>