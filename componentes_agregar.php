<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$componente = isset($_POST['componente']) ? $_POST['componente'] : null ;
$proveedor = isset($_POST['proveedor']) ? $_POST['proveedor'] : null ;
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : null ;
$unidad = isset($_POST['unidad']) ? $_POST['unidad'] : null ;
$precio_unidad = isset($_POST['precio_unidad']) ? $_POST['precio_unidad'] : null ;
?>

<?php
//agregar el componente
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM componentes WHERE componente = '$componente' and proveedor = '$proveedor' LIMIT 1");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO componentes values ('', '$ahora', '$sesion_id', '$unidad', '$componente', '$precio_unidad', '$proveedor')");
        $mensaje = "<p class='mensaje_exito'>El componente <strong>$componente</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El componente <strong>$componente</strong> ya existe, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="componentes_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Componentes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo componente</h2>
                <?php echo "$mensaje"; ?>                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <p><label for="proveedor">Proveedor:</label></p>
                    <p><select id="proveedor" name="proveedor" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los proveedores
                        $consulta = $conexion->query("SELECT * FROM proveedores ORDER BY proveedor");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_proveedor = $fila['id'];
                                $proveedor = $fila['proveedor'];
                                ?>

                                <option value="<?php echo "$id_proveedor"; ?>"><?php echo ucfirst($proveedor) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado proveedores</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="componente">Nombre del componente:</label></p>
                    <p><input type="text" id="componente" name="componente" required /></p>
                    <p><label for="unidad">Unidad de medida:</label></p>
                    <p><select id="unidad" name="unidad" required>
                        <option value=""></option>
                        <option ="cm">cm</option>
                        <option ="cm2">cm2</option>
                        <option ="cm3">cm3</option>
                        <option ="gr">gr</option>
                        <option ="kg">kg</option>
                        <option ="lb">lb</option>
                        <option ="lt">lt</option>
                        <option ="ml">ml</option>
                        <option ="mt">mt</option>
                        <option ="mt2">mt2</option>
                        <option ="mt3">mt3</option>
                        <option ="tn">tn</option>
                        <option ="unid">unid</option>
                    </select></p>
                    <p><label for="precio_unidad">Precio unitario:</label></p>
                    <p><input type="number" id="precio_unidad" name="precio_unidad" required /></p>
                    
                    
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>