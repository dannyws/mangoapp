<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;
$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$componente = isset($_POST['componente']) ? $_POST['componente'] : null ;
$proveedor = isset($_POST['proveedor']) ? $_POST['proveedor'] : null ;
$cantidad = isset($_POST['cantidad']) ? $_POST['cantidad'] : null ;
$unidad = isset($_POST['unidad']) ? $_POST['unidad'] : null ;
$precio_unidad = isset($_POST['precio_unidad']) ? $_POST['precio_unidad'] : null ;
?>

<?php
//actualizo la información del componente
if ($editar == "si")
{
    $actualizar = $conexion->query("UPDATE componentes SET fecha = '$ahora', usuario = '$sesion_id', unidad = '$unidad', componente = '$componente', precio_unidad = '$precio_unidad', proveedor = '$proveedor' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El componente <strong>$componente</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el componente.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="componentes_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Componentes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    
    <section id="contenedor">
    
        <article class="bloque">
            
            <?php
            //consulto y muestro el componente
            $consulta = $conexion->query("SELECT * FROM componentes WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Componente eliminado</h2>
                    <p class="mensaje_error">Este componente ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $unidad = $fila['unidad'];
                    $componente = $fila['componente'];
                    $precio_unidad = $fila['precio_unidad'];
                    $proveedor = $fila['proveedor'];

                    //consulto el proveedor
                    $consulta_proveedor = $conexion->query("SELECT * FROM proveedores WHERE id = '$proveedor'");           

                    if ($fila = $consulta_proveedor->fetch_assoc()) 
                    {
                        $proveedor = $fila['proveedor'];
                    }
                    else
                    {
                        $proveedor = "No se ha asignado un proveedor";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('img/iconos/componentes.jpg');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$componente"); ?></h2>
                    <div class="bloque_margen">                    
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Proveedor</span><?php echo ucfirst($proveedor) ?></p>
                        <p><span class="item_titulo">Precio unitario</span>$<?php echo number_format($precio_unidad, 0, ",", "."); ?></p>
                        <p><span class="item_titulo">Unidad de medida</span><?php echo ucfirst($unidad) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="componentes_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este componente"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>  
                   
        </article>        
    </section>
    <footer></footer>
</body>
</html>