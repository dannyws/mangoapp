# mangoapp
ManGo! es una aplicación web que te permite controlar y administrar cualquier tipo de negocio (bar, café, restaurante, discoteca, almacén, etc...) en tiempo real y desde cualquier lugar del mundo. Funciona con cualquier dispositivo conectado a internet, sin necesidad de instalar nada. ¡Maneja tu negocio desde cualquier lugar!
