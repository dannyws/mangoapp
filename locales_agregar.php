<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$local = isset($_POST['local']) ? $_POST['local'] : null ;
$direccion = isset($_POST['direccion']) ? $_POST['direccion'] : null ;
$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;
?>

<?php
//agregar el local
if ($agregar == 'si')
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
    }
    else
    {
        $imagen = "no";
    }

    $consulta = $conexion->query("SELECT local, telefono FROM locales WHERE local = '$local' and telefono = '$telefono'");

    if ($consulta->num_rows == 0)
    {
        $imagen_ref = "locales";
        $insercion = $conexion->query("INSERT INTO locales values ('', '$ahora', '$sesion_id', '$local', '$direccion', '$telefono', '$tipo', '$imagen', '$ahora_img')");
        $mensaje = "<p class='mensaje_exito'>El local <strong>$local</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;        

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El local <strong>$local</strong> ya existe, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="locales_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Locales</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo local</h2>
                <?php echo "$mensaje"; ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <p><label for="local">Local:</label></p>
                    <p><input type="text" id="local" name="local" required autofocus /></p>
                    <p><label for="direccion">Dirección:</label></p>
                    <p><input type="text" id="direccion" name="direccion" required /></p>
                    <p><label for="telefono">Teléfono:</label></p>
                    <p><input type="tel" id="telefono" name="telefono" required /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value=""></option>
                        <option value="bodega">Bodega</option>
                        <option value="punto de venta">Punto de venta</option>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body> 
</html>