<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;

$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : null ;
$imagen_nombre = isset($_POST['imagen_nombre']) ? $_POST['imagen_nombre'] : null ;
$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
$zona = isset($_POST['zona']) ? $_POST['zona'] : null ;
$producto = isset($_POST['producto']) ? $_POST['producto'] : null ;
$precio = isset($_POST['precio']) ? $_POST['precio'] : null ;
$impuesto = isset($_POST['impuesto']) ? $_POST['impuesto'] : null ;
$codigo_barras = isset($_POST['codigo_barras']) ? $_POST['codigo_barras'] : null ;
$descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : null ;

$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//actualizo la información del producto
if ($editar == "si")
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
        $imagen_nombre = $ahora_img;
        $imagen_ref = "productos";

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $imagen = $imagen;
    }

    $actualizar = $conexion->query("UPDATE productos SET fecha = '$ahora', usuario = '$sesion_id', categoria = '$categoria', tipo = '$tipo', local = '$local', zona = '$zona', producto = '$producto', precio = '$precio', impuesto = '$impuesto', descripcion = '$descripcion', codigo_barras = '$codigo_barras', imagen = '$imagen', imagen_nombre = '$imagen_nombre' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el producto.";
    }        
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="productos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Productos</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <?php
            //consulto y muestro el producto
            $consulta = $conexion->query("SELECT * FROM productos WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Producto eliminado</h2>
                    <p class="mensaje_error">Este producto ya no existe.</p>
                </div>

                <?php
            }
            else
             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $producto_id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $categoria = $fila['categoria'];
                    $tipo = $fila['tipo'];
                    $local = $fila['local'];
                    $zona = $fila['zona'];
                    $producto = $fila['producto'];
                    $precio = $fila['precio'];
                    $impuesto = $fila['impuesto'];
                    $descripcion = $fila['descripcion'];
                    $codigo_barras = $fila['codigo_barras'];
                    $imagen = $fila['imagen'];
                    $imagen_nombre = $fila['imagen_nombre'];

                    if ($imagen == "no")
                    {
                        $imagen = "img/iconos/productos.jpg";
                    }
                    else
                    {
                        $imagen = "img/avatares/productos-$producto_id-$imagen_nombre.jpg";
                    }

                    //consulto la categoria
                    $consulta_categoria = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$categoria'");           

                    if ($fila_categoria = $consulta_categoria->fetch_assoc()) 
                    {
                        $categoria = $fila_categoria['categoria'];
                    }
                    else
                    {
                        $categoria = "No se ha asignado una categoría";
                    }

                    //consulto el local
                    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

                    if ($fila = $consulta_local->fetch_assoc()) 
                    {
                        $local = $fila['local'];
                        $local_tipo = $fila['tipo'];
                    }
                    else
                    {
                        $local = "Todos los locales";
                    }

                    //consulto la zona de entregas
                    $consulta_zona = $conexion->query("SELECT * FROM zonas_entregas WHERE id = '$zona'");           

                    if ($fila = $consulta_zona->fetch_assoc()) 
                    {
                        $zona = $fila['zona'];
                    }
                    else
                    {
                        $zona = "No se ha asignado una zona de entregas";
                    }

                    //consulto el impuesto
                    $consulta_impuesto = $conexion->query("SELECT * FROM impuestos WHERE id = '$impuesto'");           

                    if ($fila_impuesto = $consulta_impuesto->fetch_assoc()) 
                    {
                        $impuesto = $fila_impuesto['impuesto'];
                        $impuesto_porcentaje = $fila_impuesto['porcentaje'];
                    }
                    else
                    {
                        $impuesto = "No se ha asignado un impuesto";
                        $impuesto_porcentaje = 0;
                    }

                    if (empty($descripcion))
                    {
                        $descripcion = "Sin descripción";
                    }

                    if (empty($codigo_barras))
                    {
                        $codigo_barras = "Sin código de barras";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }                    

                    //calculo el valor del precio venta con el impuesto incluido
                    $impuesto_valor = $precio * ($impuesto_porcentaje / 100);
                    $base_impuesto = $precio - $impuesto_valor;

                    if ($base_impuesto == $precio)
                    {
                        $base_impuesto = 0;
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('<?php echo ($imagen) ?>');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$producto"); ?></h2>
                    <div class="bloque_margen">
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Categoría</span><?php echo ucfirst($categoria) ?></p>
                        <p><span class="item_titulo">Tipo</span><?php echo ucfirst($tipo) ?></p>
                        <p><span class="item_titulo">Local en que se vende</span><?php echo ucfirst($local) ?></p>
                        <p><span class="item_titulo">Zona de entrega en el local</span><?php echo ucfirst($zona) ?></p>
                        <p><span class="item_titulo">Precio final de venta</span>$ <?php echo number_format($precio, 0, ",", "."); ?></p>
                        <p><span class="item_titulo">Base Impuesto</span>$ <?php echo number_format($base_impuesto, 0, ",", "."); ?></p>
                        <p><span class="item_titulo">Impuesto</span>$ <?php echo number_format($impuesto_valor, 0, ",", "."); ?> (<?php echo ucfirst($impuesto_porcentaje) ?> % - <?php echo ucfirst($impuesto) ?>)</p>
                        
                        <p><span class="item_titulo">Descripción</span><?php echo ucfirst($descripcion) ?></p>
                        <p><span class="item_titulo">Código de barras</span><?php echo ucfirst($codigo_barras) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>
                        <p class="alineacion_botonera"><a href="productos_editar.php?id=<?php echo "$producto_id"; ?>"><input type="button" class="proceder" value="Modificar este producto"></a></p>
                    </div>

                    <?php
                }
            }
            ?>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Composición de este producto</h2>
                <p>Estos componentes se descontarán automáticamente del inventario local cuando se hagan ventas de este producto.</p>
                <?php                
                //consulto y muestros la composición de este producto
                $consulta = $conexion->query("SELECT * FROM composiciones WHERE producto = '$producto_id' ORDER BY fecha DESC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se ha encontrado composición para este producto.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $producto_id = $fila['producto'];
                        $componente = $fila['componente'];
                        $cantidad = $fila['cantidad'];

                        //consulto el componente
                        $consulta2 = $conexion->query("SELECT * FROM componentes WHERE id = $componente");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $unidad = $filas2['unidad'];
                            $componente = $filas2['componente'];
                            $precio_unidad = $filas2['precio_unidad'];
                        }
                        else
                        {
                            $componente = "No se ha asignado un componente";
                        }
                        ?>
                        
                        <div class="item">
                            <div class="item">
                                <div class="item_img">                            
                                    <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                                </div>
                                <div class="item_info">
                                    <span class="item_titulo"><?php echo ucfirst("$cantidad"); ?> <?php echo ucfirst("$unidad"); ?> de <?php echo ucfirst("$componente"); ?></span>
                                </div>
                            </div>
                        </div>
                       
                        <?php
                    }
                }
                ?>

                <p class="alineacion_botonera"><a href="productos_componentes.php?id=<?php echo "$producto_id"; ?>"><input type="button" class="proceder" value="Editar componentes"></a></p>

            </div>
        </article>



    </section>
    <footer></footer>
</body>
</html>