<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
    <meta http-equiv="refresh" content="10" />
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="index.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt"> Inicio</span></h2>
                </div>
            </a>
            <a href="index.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">

                <h2>Zonas de entregas en <?php echo ucwords($sesion_local); ?></h2>

                <?php
                //consulto y muestro las zonas de entregas activas en el local
                $consulta = $conexion->query("SELECT distinct local, zona FROM ventas_productos WHERE local = '$sesion_local_id' and estado = 'pedido' ORDER BY zona");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No hay productos o servicios pendientes para entregar.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una zona de entrega para para visualizar los pedidos.</p>

                    <?php

                    while ($fila = $consulta->fetch_assoc())
                    {
                        $local = $fila['local'];
                        $zona = $fila['zona'];

                        //consulto el total de productos pedidos en la zona
                        $consulta_productos = $conexion->query("SELECT * FROM ventas_productos WHERE zona = '$zona' and local = '$sesion_local_id' and estado = 'pedido'");
                        $total_productos = $consulta_productos->num_rows;

                        //consulto el ultimo pedido hecho
                        $consulta_ultimo = $conexion->query("SELECT * FROM ventas_productos WHERE zona = '$zona' and local = '$sesion_local_id' and estado = 'pedido' ORDER BY fecha DESC LIMIT 1");

                        while ($fila = $consulta_ultimo->fetch_assoc())
                        {
                            $fecha = date('d M', strtotime($fila['fecha']));
                            $hora = date('h:i a', strtotime($fila['fecha']));
                        }

                        //consulto la zona
                        $consulta_zona = $conexion->query("SELECT * FROM zonas_entregas WHERE id = '$zona'");           

                        if ($fila = $consulta_zona->fetch_assoc()) 
                        {
                            $zona_id = $fila['id'];
                            $zona = $fila['zona'];
                        }
                        else
                        {
                            $zona_id = 0;
                            $zona = "Principal";
                        }
                    
                        ?>

                        <a href="zonas_entregas_resumen.php?zona_id=<?php echo "$zona_id";?>&zona=<?php echo "$zona";?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('img/iconos/zonas_entregas.jpg');"></div>                                        
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$zona"); ?></span>
                                        <span class="item_descripcion"><?php echo ucfirst("$total_productos"); ?> pedidos</span>
                                        <span class="item_descripcion">Último pedido a las <?php echo $hora; ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <?php
                    }
                }
                ?>                
            </div>

        </article>

    </section>

    <footer></footer>

</body>
</html>