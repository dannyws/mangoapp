<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;
?>

<?php
//agregar la categoria
if ($agregar == 'si')
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
    }
    else
    {
        $imagen = "no";
    }

    $consulta = $conexion->query("SELECT categoria FROM productos_categorias WHERE categoria = '$categoria'");

    if ($consulta->num_rows == 0)
    {
        $imagen_ref = "categorias";
        $insercion = $conexion->query("INSERT INTO productos_categorias values ('', '$ahora', '$sesion_id', '$categoria', '$tipo', '$imagen', '$ahora_img')");
        $mensaje = "<p class='mensaje_exito'>La categoría <strong>$categoria</strong> fue agregada exitosamente.</p>";

        $id = $conexion->insert_id;        

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>La categoría <strong>$categoria</strong> ya existe, no es posible agregarla de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="categorias_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Categorías</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar una nueva categoría</h2>
                <?php echo "$mensaje"; ?>                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <p><label for="categoria">Categoría:</label></p>
                    <p><input type="text" id="categoria" name="categoria" required autofocus /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value=""></option>
                        <option value="productos">Productos</option>
                        <option value="servicios">Servicios</option>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body> 
</html>