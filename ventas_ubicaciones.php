<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$pagar = isset($_GET['pagar']) ? $_GET['pagar'] : null ;
$eliminar_venta = isset($_GET['eliminar_venta']) ? $_GET['eliminar_venta'] : null ;
$ubicacion = isset($_GET['ubicacion']) ? $_GET['ubicacion'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$venta_total_bruto = isset($_GET['venta_total_bruto']) ? $_GET['venta_total_bruto'] : null ;
$descuento_valor = isset($_GET['descuento_valor']) ? $_GET['descuento_valor'] : null ;
$venta_total_neto = isset($_GET['venta_total_neto']) ? $_GET['venta_total_neto'] : null ;
$venta_total = isset($_GET['venta_total']) ? $_GET['venta_total'] : null ;
$venta_id = isset($_GET['venta_id']) ? $_GET['venta_id'] : null ;
$ubicacion_id = isset($_GET['ubicacion_id']) ? $_GET['ubicacion_id'] : null ;
?>

<?php
//elimino la venta
if ($eliminar_venta == 'si')
{
    $borrar_venta = $conexion->query("DELETE FROM ventas_datos WHERE id = $venta_id");

    if ($borrar_venta)
    {
        $borrar_venta_productos = $conexion->query("DELETE FROM ventas_productos WHERE venta_id = $venta_id");
        $actualizar = $conexion->query("UPDATE ubicaciones SET estado = 'libre' WHERE id = '$ubicacion_id'");
        
        $mensaje = "<p class='mensaje_exito'>La venta <strong>No $venta_id</strong> fue eliminada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar la venta <strong>$venta_id</strong>.</p>";
    }
}
?>

<?php
//liquido la venta
if ($pagar == "si")
{
    $actualizar = $conexion->query("UPDATE ventas_datos SET estado = 'liquidado', total_bruto = '$venta_total_bruto', descuento_valor = '$descuento_valor', total_neto = '$venta_total_neto' WHERE id = '$venta_id'");
    $actualizar = $conexion->query("UPDATE ubicaciones SET estado = 'libre' WHERE id = '$ubicacion_id'");
    $mensaje = "<p class='mensaje_exito'>La venta <strong>No $venta_id</strong> fue liquidada exitosamente.</p>"; 
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
    <meta http-equiv="refresh" content="30" />
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="index.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt"> Inicio</span></h2>
                </div>
            </a>
            <a href="index.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">

                <h2>Ubicaciones en <?php echo ucwords($sesion_local); ?></h2>

                <?php
                //consulto y muestro las ubicaciones relacionadas a mi local
                $consulta = $conexion->query("SELECT * FROM ubicaciones WHERE local = '$sesion_local_id' ORDER BY ubicacion");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han agregado ubicaciones al local con el que estás relacionado.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una ubicación para comenzar una venta.</p>
                    <?php echo "$mensaje";?>

                    <?php

                    while ($fila = $consulta->fetch_assoc())
                    {
                        $ubicacion_id = $fila['id'];
                        $ubicacion = $fila['ubicacion'];
                        $ubicada = $fila['ubicada'];
                        $estado = $fila['estado'];
                        $tipo = $fila['tipo'];

                        //consulto el id de la venta en esta ubicación
                        $consulta_venta = $conexion->query("SELECT * FROM ventas_datos WHERE ubicacion_id = '$ubicacion_id' and estado = 'ocupado'");

                        if ($consulta_venta->num_rows != 0)
                        {
                            while ($fila_venta = $consulta_venta->fetch_assoc())
                            {
                                $venta_id = $fila_venta['id'];
                                $venta_fecha = date('d M', strtotime($fila_venta['fecha']));
                                $venta_hora = date('h:i a', strtotime($fila_venta['fecha']));
                                $venta_usuario = $fila_venta['usuario_id'];                                

                                //consulto el usuario que tiene la venta
                                $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$venta_usuario'");           

                                if ($fila = $consulta_usuario->fetch_assoc()) 
                                {
                                    $nombres = $fila['nombres'];
                                    $apellidos = $fila['apellidos'];
                                    $atendido = "Atendido por: ".ucwords($nombres)." ".ucwords($apellidos)."";
                                    $recoger = "Recoger:";
                                }
                            }

                            $venta_desde = "desde las $venta_hora";
                            $venta_class = "item_img_top";

                            //consulto el total de los productos ingresados a la venta   
                            $consulta_venta_total = $conexion->query("SELECT * FROM ventas_productos WHERE venta_id = '$venta_id'");

                            if ($consulta_venta_total->num_rows != 0)
                            {

                                while ($fila_venta_total = $consulta_venta_total->fetch_assoc())
                                {
                                    $precio = $fila_venta_total['precio_final'];

                                    $venta_total = $venta_total + $precio;
                                }
                                $venta_total = "Venta actual: $ ".number_format($venta_total, 0, ",", ".");                                
                            }
                            else
                            {
                               $venta_total = ""; 
                            }
                        }
                        else
                        {
                            $venta_id = 0;
                            $venta_desde = "";
                            $venta_total = "";
                            $venta_class = "item_img";
                            $atendido = "";
                            $recoger = "";
                        }
                        ?>

                        <a href="ventas_categorias.php?ubicacion_id=<?php echo "$ubicacion_id";?>&ubicacion=<?php echo "$ubicacion";?>&ubicacion_tipo=<?php echo "$tipo";?>">
                            <div class="item">
                                <div class="item">
                                    <div class="<?php echo "$venta_class";?>">
                                        <div class="img_avatar" style="background-image: url('img/iconos/<?php echo "$tipo"; ?>_<?php echo "$estado"; ?>.jpg');"></div>                                        
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$ubicacion"); ?></span>
                                        <span class="item_descripcion"><?php echo ucfirst("$ubicada"); ?></span>
                                        <span class="item_descripcion_claro"><?php echo ucfirst("$estado"); ?> <?php echo "$venta_desde"; ?></span>
                                        <span class="item_descripcion_claro"><?php echo "$venta_total"; ?></span>
                                        <span class="item_descripcion_claro"><?php echo "$atendido"; ?></span>
                                        <?php 
                                        //productos en estado entregado
                                        $consulta_entregados = $conexion->query("SELECT * FROM ventas_productos WHERE venta_id = '$venta_id' and estado = 'entregado'");           

                                        while ($fila = $consulta_entregados->fetch_assoc()) 
                                        {
                                            $producto_entregado = $fila['producto'];
                                            $zona_entregado = $fila['zona'];

                                            //consulto la zona
                                            $consulta_zona = $conexion->query("SELECT * FROM zonas_entregas WHERE id = '$zona_entregado'");           

                                            if ($fila = $consulta_zona->fetch_assoc()) 
                                            {
                                                $zona_entregado = $fila['zona'];
                                            }
                                            else
                                            {
                                                $zona_entregado = "Principal";
                                            }

                                            ?>

                                            <span class="item_descripcion_claro">Recoger <span class='texto_exito'><?php echo ucfirst($producto_entregado); ?></span> en <?php echo ucfirst($zona_entregado); ?></span>
                                            
                                            <?php                                            
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <?php
                    }
                }
                ?>                
            </div>

        </article>

    </section>

    <footer></footer>

</body>
</html>