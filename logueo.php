<?php
//inicio la sesión
session_start();
?>

<?php
//verifico si la sesión está creada y si lo está se envia al index
if (isset($_SESSION['correo']))
{
    header("location:index.php");
}
?>

<?php
//capturo las variables que pasan por URL desde validacion.php
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$correo = isset($_GET['correo']) ? $_GET['correo'] : null ;
$men = isset($_GET['men']) ? $_GET['men'] : null ;
?>

<?php
//mensajes del inicio de sesion
if ($men == 1)
{
    $mensaje = "<p class='mensaje_error'>Tu correo <strong>$correo</strong> no ha sido registrado, crea una cuenta para acceder.</p>";
}

if ($men == 2)
{
    $mensaje = "<p class='mensaje_error'>Contraseña incorrecta.</p>";
}

if ($men == 3)
{
    $mensaje = "<p class='mensaje_exito'>Salida exitosa. Regresa pronto!</p>";
}
//fin mensajes del inicio de sesion
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href="img/sis/favicon.ico" />
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/estilos.css" />
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h3></h3>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Hola! que bueno que volviste.</h2>
                <p>Escríbe tu correo y tu contraseña para ingresar a ManGo!</p>
                <?php echo "$mensaje"; ?>
                <form action="logueo_validacion.php" method="post">
                    <p><label for="correo">Correo:</label></p>
                    <p><input type="email" id="correo" name="correo" value="<?php echo "$correo"; ?>" required autofocus></p>
                    <p><label for="contrasena">Contraseña:</label></p>
                    <p><input type="password" id="contrasena" name="contrasena" required></p>
                    
                    <p><button type="submit" class="proceder">Acceder</button></p>
                </form>
            </div>
        </article>
    <footer></footer>
</body>
</html>