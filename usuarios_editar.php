<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del usuario
$consulta = $conexion->query("SELECT * FROM usuarios WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $correo = $fila['correo'];
    $contrasena = $fila['contrasena'];
    $nombres = $fila['nombres'];
    $apellidos = $fila['apellidos'];
    $tipo = $fila['tipo'];
    $local = $fila['local'];
    $imagen = $fila['imagen'];
    $imagen_nombre = $fila['imagen_nombre'];

    //consulto el local
    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

    if ($fila = $consulta_local->fetch_assoc()) 
    {
        $local_id = $fila['id'];
        $local = $fila['local'];
        $local_tipo = $fila['tipo'];
    }
    else
    {
        $local_id = "";
        $local = "No se ha asignado un local";
        $local_tipo = "";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="usuarios_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Usuario</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este usuario</h2>                
                <form action="usuarios_detalle.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>" />
                    <input type="hidden" name="imagen" value="<?php echo "$imagen"; ?>" />
                    <input type="hidden" name="imagen_nombre" value="<?php echo "$imagen_nombre"; ?>" />
                    <p><label for="correo">Correo:</label></p>
                    <p><input type="email" id="correo" name="correo" value="<?php echo "$correo"; ?>" required autofocus /></p>
                    <p><label for="contrasena">Contraseña:</label></p>
                    <p><input type="text" id="contrasena" name="contrasena" value="<?php echo "$contrasena"; ?>" required /></p>
                    <p><label for="nombres">Nombres:</label></p>
                    <p><input type="text" id="nombres" name="nombres" value="<?php echo "$nombres"; ?>" required /></p>
                    <p><label for="apellidos">Apellidos:</label></p>
                    <p><input type="text" id="apellidos" name="apellidos" value="<?php echo "$apellidos"; ?>" required /></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst("$tipo"); ?></option>
                        <option value=""></option>
                        <option value="administrador">Administrador</option>
                        <option value="mesero">Mesero</option>
                        <option value="socio">Socio</option>
                        <option value="vendedor">Vendedor</option>
                    </select></p>
                    <p><label for="local">Local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value="<?php echo "$local_id"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($local_tipo) ?>)</option>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo"  /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="usuarios_ver.php?eliminar=si&id=<?php echo "$id"; ?>&correo=<?php echo "$correo"; ?>"><input type="button" class="advertencia" value="Eliminar este usuario"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>