<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$entregar = isset($_GET['entregar']) ? $_GET['entregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$zona_id = isset($_GET['zona_id']) ? $_GET['zona_id'] : null ;
$zona = isset($_GET['zona']) ? $_GET['zona'] : null ;

$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//entrego el producto o servicio del pedido
if ($entregar == "si")
{
    $actualizar = $conexion->query("UPDATE ventas_productos SET estado = 'entregado' WHERE id = '$id'");
    $mensaje = "<p class='mensaje_exito'>Producto entregado exitosamente.</p>";
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
    <meta http-equiv="refresh" content="30" />
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="zonas_entregas_entrada.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt"> Z. de entregas</span></h2>
                </div>
            </a>
            <a href="ventas_ubicaciones.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ventas_descuentos.php?venta_id=<?php echo "$venta_id";?>">
                <div class="cabezote_col_der">
                    <h2></h2>
                </div>
            </a>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">

            <div class="bloque_margen">

                <h2><span class="descripcion"><?php echo ucfirst($zona) ;?> / </span>Pedidos pendientes</h2>
                <?php
                //consulto y muestro los productos o servicios pedidos en esta zona
                $consulta = $conexion->query("SELECT * FROM ventas_productos WHERE zona = '$zona_id' and local = '$sesion_local_id' and estado = 'pedido' ORDER BY fecha, ubicacion ASC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han agregado pedidos a esta zona de entregas.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca el producto cuando desees entregarlo.</p>
                    <?php echo "$mensaje"; ?> 
                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id']; 
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $ubicacion = $fila['ubicacion'];
                        $producto = $fila['producto_id'];                        
                        $categoria = $fila['categoria'];

                        //consulto los datos del producto
                        $consulta_producto = $conexion->query("SELECT * FROM productos WHERE id = '$producto'");           

                        if ($fila = $consulta_producto->fetch_assoc()) 
                        {
                            $producto_id = $fila['id'];
                            $producto = $fila['producto'];
                            $imagen = $fila['imagen'];

                            //consulto la imagen del producto
                            $consulta_producto_img = $conexion->query("SELECT * FROM productos WHERE id = '$producto_id'");                        

                            while ($fila_producto_img = $consulta_producto_img->fetch_assoc())
                            {
                                $imagen = $fila_producto_img['imagen'];
                                $imagen_nombre = $fila_producto_img['imagen_nombre'];

                                if ($imagen == "no")
                                {
                                    $imagen = "img/iconos/productos-m.jpg";
                                }
                                else
                                {
                                    $imagen = "img/avatares/productos-$producto_id-$imagen_nombre-m.jpg";
                                }
                            }
                        }
                                                
                        ?>

                        <a href="zonas_entregas_resumen.php?entregar=si&id=<?php echo $id ?>&zona_id=<?php echo "$zona_id";?>&zona=<?php echo "$zona";?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img_top">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen"; ?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo $hora; ?> / <?php echo ucfirst("$producto"); ?></span>
                                        <span class="item_descripcion_claro"><?php echo ucfirst("$categoria"); ?></span>
                                        <span class="item_descripcion_claro"><?php echo ucfirst("$ubicacion"); ?></span>                                
                                    </div>
                                </div>
                            </div>
                        </a>

                        <?php
                    }
                }
                ?>
                                
            </div>

        </article>

    </section>

    <footer></footer>

</body>
</html>