<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del impuesto
$consulta = $conexion->query("SELECT * FROM impuestos WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $impuesto = $fila['impuesto'];
    $porcentaje = $fila['porcentaje'];
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="impuestos_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Impuesto</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este impuesto</h2>
                <form action="impuestos_detalle.php" method="post">
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                    <p><label for="impuesto">Impuesto:</label></p>
                    <p><input type="text" id="impuesto" name="impuesto" value="<?php echo "$impuesto"; ?>" required autofocus /></p>
                    <p><label for="porcentaje">Porcentaje:</label></p>
                    <p><input type="number" min="0" max="100" id="porcentaje" name="porcentaje" value="<?php echo "$porcentaje"; ?>" required /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="impuestos_ver.php?eliminar=si&id=<?php echo "$id"; ?>&impuesto=<?php echo "$impuesto"; ?>"><input type="button" class="advertencia" value="Eliminar este impuesto"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>