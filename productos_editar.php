<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información del producto
$consulta = $conexion->query("SELECT * FROM productos WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i a', strtotime($fila['fecha']));
    $usuario = $fila['usuario'];
    $categoria = $fila['categoria'];
    $tipo = $fila['tipo'];
    $local = $fila['local'];
    $zona = $fila['zona'];
    $producto = $fila['producto'];
    $precio = $fila['precio'];
    $impuesto = $fila['impuesto'];
    $descripcion = $fila['descripcion'];
    $codigo_barras = $fila['codigo_barras'];
    $imagen = $fila['imagen'];
    $imagen_nombre = $fila['imagen_nombre'];

    //consulto el impuesto
    $consulta_impuesto = $conexion->query("SELECT * FROM impuestos WHERE id = '$impuesto'");           

    if ($fila_impuesto = $consulta_impuesto->fetch_assoc()) 
    {
        $id_impuesto = $fila_impuesto['id'];
        $impuesto = $fila_impuesto['impuesto'];
        $porcentaje = $fila_impuesto['porcentaje'];
    }
    else
    {
        $id_impuesto = "";
        $impuesto = "No se ha asignado un impuesto";
        $porcentaje = 0;
    }

    //consulto la categoría
    $consulta_categoria = $conexion->query("SELECT * FROM productos_categorias WHERE id = '$categoria'");           

    if ($fila_categoria = $consulta_categoria->fetch_assoc()) 
    {
        $id_categoria = $fila_categoria['id'];
        $categoria = $fila_categoria['categoria'];
    }
    else
    {
        $id_categoria = "";
        $categoria = "No se ha asignado una categoria";
    }

    //consulto el local
    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

    if ($fila = $consulta_local->fetch_assoc()) 
    {
        $local_id = $fila['id'];
        $local = $fila['local'];
        $local_tipo = "(".ucfirst($fila['tipo']).")";
    }
    else
    {
        $local_id = "0";
        $local = "Todos los locales";
        $local_tipo = "Todos";
    }

    //consulto la zona de entregas
    $consulta_zona = $conexion->query("SELECT * FROM zonas_entregas WHERE id = '$zona'");           

    if ($fila = $consulta_zona->fetch_assoc()) 
    {
        $zona_id = $fila['id'];
        $zona = $fila['zona'];
    }
    else
    {
        $zona_id = "0";
        $zona = "No se ha asignado una zona de entregas";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="productos_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Producto</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar este producto</h2>                
                <form action="productos_detalle.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>" />
                    <input type="hidden" name="imagen" value="<?php echo "$imagen"; ?>" />
                    <input type="hidden" name="imagen_nombre" value="<?php echo "$imagen_nombre"; ?>" />
                    <p><label for="categoria">Categoría:</label></p>
                    <p><select id="categoria" name="categoria" required autofocus>
                        <option value="<?php echo "$id_categoria"; ?>"><?php echo ucfirst($categoria); ?></option>
                        <option value=""></option>
                        <?php
                        //consulto y muestro las categorias
                        $consulta = $conexion->query("SELECT * FROM productos_categorias ORDER BY categoria");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_categoria = $fila['id'];
                                $categoria = $fila['categoria'];
                                ?>

                                <option value="<?php echo "$id_categoria"; ?>"><?php echo ucfirst($categoria) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado categorías</option>

                            <?php
                        }
                        ?>
                    </select></p>

                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst($tipo) ?></option>
                        <option value=""></option>
                        <option value="compuesto">Compuesto</option>
                        <option value="simple">Simple</option>                        
                    </select></p>

                    <p><label for="local">Local en que se vende:</label></p>
                    <p><select id="local" name="local" required>
                        <option value="<?php echo "$local_id"; ?>"><?php echo ucfirst($local) ?> <?php echo ucfirst($local_tipo) ?></option>
                        <option value=""></option>
                        <option value="0">Todos los locales</option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>

                    <p><label for="zona">Zona de entrega en el local:</label></p>
                    <p><select id="zona" name="zona" required>
                        <option value="<?php echo "$zona_id"; ?>"><?php echo ucfirst($zona) ?></option>
                        <option value=""></option>
                        <?php
                        //consulto y muestro las zonas de entregas
                        $consulta = $conexion->query("SELECT * FROM zonas_entregas");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_zona = $fila['id'];
                                $zona = $fila['zona'];                               

                                ?>

                                <option value="<?php echo "$id_zona"; ?>"><?php echo ucfirst($zona) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado zonas de entregas</option>

                            <?php
                        }
                        ?>
                    </select></p>

                    <p><label for="producto">Producto:</label></p>
                    <p><input type="text" id="producto" name="producto" value="<?php echo "$producto"; ?>" required /></p>
                    <p><label for="precio">Precio final de venta:</label></p>
                    <p><input type="number" step="100" id="precio" name="precio" value="<?php echo "$precio"; ?>" required /></p>
                    <p><label for="impuesto">Impuesto:</label></p>
                    <p><select id="impuesto" name="impuesto" required>
                        <option value="<?php echo "$id_impuesto"; ?>"><?php echo ucfirst($porcentaje) ?> % - <?php echo ucfirst($impuesto); ?></option>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los impuestos
                        $consulta = $conexion->query("SELECT * FROM impuestos ORDER BY porcentaje");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_impuesto = $fila['id'];
                                $impuesto = $fila['impuesto'];
                                $porcentaje = $fila['porcentaje'];
                                ?>

                                <option value="<?php echo "$id_impuesto"; ?>"><?php echo "$porcentaje"; ?> % - <?php echo ucfirst($impuesto) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado impuestos</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="descripcion">Descripción:</label></p>
                    <p><textarea id="descripcion" name="descripcion"><?php echo "$descripcion"; ?></textarea></p>
                    <p><label for="codigo_barras">Código de barras (usa un lector de código de barras):</label></p>
                    <p><input type="number" id="codigo_barras" name="codigo_barras" value="<?php echo "$codigo_barras"; ?>" /></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="productos_ver.php?eliminar=si&id=<?php echo "$id"; ?>&producto=<?php echo "$producto"; ?>"><input type="button" class="advertencia" value="Eliminar este producto"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>