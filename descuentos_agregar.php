<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$descuento = isset($_POST['descuento']) ? $_POST['descuento'] : null ;
$porcentaje = isset($_POST['porcentaje']) ? $_POST['porcentaje'] : null ;
?>

<?php
//agregar el descuento
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM descuentos WHERE descuento = '$descuento' and porcentaje = '$porcentaje'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO descuentos values ('', '$ahora', '$sesion_id', '$descuento', '$porcentaje')");
        $mensaje = "<p class='mensaje_exito'>El descuento <strong>$descuento</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El descuento <strong>$descuento</strong> ya existe, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="descuentos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Descuentos</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo descuento</h2>
                <?php echo "$mensaje"; ?>                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <p><label for="descuento">Descuento:</label></p>
                    <p><input type="text" id="descuento" name="descuento" required autofocus /></p>
                    <p><label for="porcentaje">Porcentaje:</label></p>
                    <p><input type="number" min="0" max="100" id="porcentaje" name="porcentaje" required /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>