<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$destino = isset($_POST['destino']) ? $_POST['destino'] : null ;
?>

<?php
//crear el despacho
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM despachos WHERE destino = '$destino' and estado = 'creado'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO despachos values ('', '$ahora', '$sesion_id', '$destino', 'creado')");       

        $id = $conexion->insert_id;
        $mensaje = "<p class='mensaje_exito'>Es despacho <strong>No $id</strong> fue creado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El <strong>local destino</strong> tiene un despacho pendiente, no es posible crear un nuevo despacho hasta que se haya recibo el despacho anterior.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="index.php"><div class="flecha_izq"></div> <span class="logo_txt"> Inicio</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">        

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Despachos enviados</h2>
                
                <?php
                //consulto los despachos creados                
                $consulta = $conexion->query("SELECT * FROM despachos WHERE estado = 'enviado' and destino = '$sesion_local_id' ORDER BY fecha DESC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado <strong>despachos enviados</strong> a este local o punto de venta.</p>

                    <?php
                }

                else
                {   ?>

                    <p>Toca un despacho para recibir los componentes en tu local o punto de venta.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc()) 
                    {
                        $id = $fila['id'];
                        $fecha = date('Y/m/d', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $destino = $fila['destino'];
                        $estado = $fila['estado'];

                        //consulto el local destino
                        $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $destino");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $destino = $filas2['local'];
                        }
                        else
                        {
                            $destino = "No se ha asignado un local destino";
                        }

                        //cantidad de componentes en este despacho                        
                        $consulta_cantidad = $conexion->query("SELECT * FROM despachos_componentes WHERE despacho_id = '$id' and estado = 'enviado'");

                        if ($consulta_cantidad->num_rows == 0)
                        {
                            $cantidad = "0 componentes";
                        }
                        else
                        {
                            $cantidad = $consulta_cantidad->num_rows;
                            $cantidad = "$cantidad componentes";
                        }
                        
                        ?>
                        <a href="inventario_componentes.php?despacho_id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img_top">
                                        <div class="img_avatar" style="background-image: url('img/iconos/despachos.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo">Para: <?php echo ucfirst("$destino"); ?></span>
                                        <span class="item_descripcion">Despacho No <?php echo ("$id"); ?></span>
                                        <span class="item_descripcion"><?php echo ("$cantidad"); ?></span>
                                        <span class="item_descripcion"><?php echo ucfirst("$estado"); ?> el <?php echo ucfirst("$fecha"); ?> a las <?php echo ucfirst("$hora"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>










        <article class="bloque">
            <div class="bloque_margen">
                <h2>Inventario en este local</h2>
                <?php echo "$mensaje"; ?>
                <?php
                //consulto los componentes
                $consulta = $conexion->query("SELECT * FROM inventario WHERE local_id = '$sesion_local_id' ORDER BY cantidad DESC");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado <strong>componentes</strong> en el inventario de este local o punto de venta.</p>

                    <?php
                }

                else
                {   ?>

                    <p>Componentes en el inventario de este local.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc()) 
                    {
                        $despacho_componente_id = $fila['id'];
                        $fecha = date('Y/m/d', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $componente_id = $fila['componente_id'];
                        $cantidad = $fila['cantidad'];

                        //consulto el componente
                        $consulta2 = $conexion->query("SELECT * FROM componentes WHERE id = $componente_id");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $componente = $filas2['componente'];
                            $unidad = $filas2['unidad'];
                        }
                        else
                        {
                            $componente = "No se ha asignado un componente";
                        }
                        
                        ?>
                        
                        <div class="item">
                            <div class="item">
                                <div class="item_img">
                                    <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                                </div>
                                <div class="item_info">
                                    <span class="item_titulo"><?php echo ucfirst("$cantidad"); ?> <?php echo ucfirst("$unidad"); ?> de <?php echo ucfirst("$componente"); ?></span>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                    }
                }
                ?>
            </div>
        </article>


    </section>
    <footer></footer>
</body>
</html>