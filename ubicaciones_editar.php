<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$id = isset($_GET['id']) ? $_GET['id'] : null ;
?>

<?php
//consulto la información de la ubicación
$consulta = $conexion->query("SELECT * FROM ubicaciones WHERE id = '$id'");

if ($fila = $consulta->fetch_assoc()) 
{
    $id = $fila['id'];
    $fecha = date('d M', strtotime($fila['fecha']));
    $hora = date('h:i:s a', strtotime($fila['fecha']));
    $ubicacion = $fila['ubicacion'];
    $ubicada = $fila['ubicada'];
    $estado = $fila['estado'];
    $tipo = $fila['tipo'];
    $local = $fila['local'];

    //consulto el local
    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

    if ($fila = $consulta_local->fetch_assoc()) 
    {
        $local_id = $fila['id'];
        $local = $fila['local'];
        $local_tipo = $fila['tipo'];
    }
    else
    {
        $local_id = "";
        $local = "No se ha asignado un local";
        $local_tipo = "";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ubicaciones_detalle.php?id=<?php echo "$id"; ?>"><div class="flecha_izq"></div> <span class="logo_txt"> Ubicación</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Modificar esta ubicación</h2>
                <form action="ubicaciones_detalle.php" method="post">
                    <input type="hidden" name="id" value="<?php echo "$id"; ?>">
                    <p><label for="ubicacion">Ubicación:</label></p>
                    <p><input type="text" id="ubicacion" name="ubicacion" value="<?php echo "$ubicacion"; ?>" required autofocus /></p>
                    <p><label for="ubicada">Ubicada en:</label></p>
                    <p><input type="text" id="ubicada" name="ubicada" value="<?php echo "$ubicada"; ?>" required /></p>
                    <p><label for="estado">Estado:</label></p>
                    <p><select id="estado" name="estado" required>
                        <option value="<?php echo "$estado"; ?>"><?php echo ucfirst($estado) ?></option>
                        <option value=""></option>
                        <option value="libre">Libre</option>
                        <option value="ocupado">Ocupado</option>
                    </select></p>
                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value="<?php echo "$tipo"; ?>"><?php echo ucfirst($tipo) ?></option>
                        <option value=""></option>
                        <option value="caja">Caja</option>
                        <option value="barra">Barra</option>
                        <option value="habitacion">Habitación</option>
                        <option value="mesa">Mesa</option>
                    </select></p>
                    <p><label for="local">Local:</label></p>
                    <p><select id="local" name="local" required>
                        <option value="<?php echo "$local_id"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($local_tipo) ?>)</option>
                        <option value=""></option>
                    <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="editar" value="si">Guardar cambios</button> <a href="ubicaciones_ver.php?eliminar=si&id=<?php echo "$id"; ?>&ubicacion=<?php echo "$ubicacion"; ?>"><input type="button" class="advertencia" value="Eliminar esta ubicación"></a></p>
                </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>