<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;

$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : null ;
$imagen_nombre = isset($_POST['imagen_nombre']) ? $_POST['imagen_nombre'] : null ;
$proveedor = isset($_POST['proveedor']) ? $_POST['proveedor'] : null ;
$correo = isset($_POST['correo']) ? $_POST['correo'] : null ;
$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : null ;
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//actualizo la información del proveedor
if ($editar == "si")
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
        $imagen_nombre = $ahora_img;
        $imagen_ref = "proveedores";

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $imagen = $imagen;
        $imagen_nombre = $imagen_nombre;
    }

    $actualizar = $conexion->query("UPDATE proveedores SET fecha = '$ahora', usuario = '$sesion_id', proveedor = '$proveedor', correo = '$correo', telefono = '$telefono', imagen = '$imagen', imagen_nombre = '$imagen_nombre' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El proveedor <strong>$proveedor</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el proveedor.";
    }        
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="proveedores_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Proveedores</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">
        <article class="bloque">
            <?php
            //consulto y muestro el proveedor
            $consulta = $conexion->query("SELECT * FROM proveedores WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Proveedor eliminado</h2>
                    <p class="mensaje_error">Este proveedor ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $proveedor = $fila['proveedor'];
                    $correo = $fila['correo'];
                    $telefono = $fila['telefono'];
                    $imagen = $fila['imagen'];
                    $imagen_nombre = $fila['imagen_nombre'];

                    if ($imagen == "no")
                    {
                        $imagen = "img/iconos/proveedores.jpg";
                    }
                    else
                    {
                        $imagen = "img/avatares/proveedores-$id-$imagen_nombre.jpg";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('<?php echo ($imagen) ?>');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$proveedor"); ?></h2>
                    <div class="bloque_margen">                        
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Correo</span><?php echo ($correo) ?></p>
                        <p><span class="item_titulo">Teléfono</span><?php echo ucfirst($telefono) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="proveedores_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este proveedor"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>
        </article>


        <article class="bloque">
            <div class="bloque_margen">
                <h2>Componentes relacionados a este proveedor</h2>                
                <?php
                //consulto los componentes
                $consulta = $conexion->query("SELECT * FROM componentes WHERE proveedor = '$id' ORDER BY componente");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado <strong>componentes</strong> relacionados a este proveedor.</p>

                    <?php
                }
                else
                {   
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $unidad = $fila['unidad'];
                        $componente = $fila['componente'];
                        $precio_unidad = $fila['precio_unidad'];
                        ?>
                                                    
                        <div class="item">
                            <div class="item">
                                <div class="item_img">                            
                                    <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                                </div>
                                <div class="item_info">
                                    <span class="item_titulo"><?php echo ucfirst("$componente"); ?></span>
                                    <span class="item_descripcion">Precio unidad: $<?php echo number_format($precio_unidad, 0, ",", "."); ?> x <?php echo ucfirst("$unidad"); ?></span>
                                </div>
                            </div>
                        </div>
                       
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>

    </section>
    <footer></footer>
</body>
</html>