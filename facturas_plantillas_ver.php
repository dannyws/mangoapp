<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$nombre = isset($_GET['nombre']) ? $_GET['nombre'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino la plantilla de factura
if ($eliminar == "si")
{
    $borrar = $conexion->query("DELETE FROM facturas_plantillas WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>La plantilla de factura <strong>$nombre</strong> fue eliminada exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar la plantilla de factura <strong>$nombre</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#financiero"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/facturas_plantillas.jpg');"></div>
            <h2 class="cab_texto">Plantillas de facturas</h2>
            <div class="bloque_margen">
                <p>Acá podrás crear el formato de las facturas que generan tus productos y servicios. Puedes modificar su titulo, encabezado, pie de página, razón social, resolución de facturación, etc.</p>
                <p class="alineacion_botonera"><a href="facturas_plantillas_agregar.php"><input type="button" class="proceder" value="Agregar una nueva plantilla de factura"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Plantillas de facturas agregadas</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar una plantilla de factura" /></p>                    
                </form>
                <?php
                //consulto el local previamente para la busqueda
                $consulta0 = $conexion->query("SELECT * FROM locales WHERE local like '%$busqueda%'");

                if ($filas0 = $consulta0->fetch_assoc())
                {
                    $local = $filas0['id'];
                }
                else
                {
                    $local = "bullshit";
                }

                //consulto y muestro las plantillas de facturas
                $consulta = $conexion->query("SELECT * FROM facturas_plantillas WHERE nombre like '%$busqueda%' or titulo like '%$busqueda%' or texto_superior like '%$busqueda%' or texto_inferior like '%$busqueda%' or local like '%$local%' ORDER BY local, nombre");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado plantillas de facturas para esta búsqueda.</p>

                    <?php
                }
                else
                {
                    ?>

                    <p>Toca una plantilla de factura para verla o editarla.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $nombre = $fila['nombre'];
                        $titulo = $fila['titulo'];
                        $texto_superior = $fila['texto_superior'];
                        $texto_inferior = $fila['texto_inferior'];
                        $local = $fila['local'];

                        //consulto el local
                        $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $local");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $local = $filas2['local'];
                        }
                        else
                        {
                            $local = "Todos los locales";
                        }                      
                        ?>
                        <a href="facturas_plantillas_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/facturas_plantillas.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$nombre"); ?></span>
                                        <span class="item_descripcion">Titulo: <?php echo ucfirst("$titulo"); ?></span>
                                        <span class="item_descripcion">Local: <?php echo ucfirst("$local"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>  

    </section>
    <footer></footer>
</body>
</html>