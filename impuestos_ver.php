<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$impuesto = isset($_GET['impuesto']) ? $_GET['impuesto'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el impuesto
if ($eliminar == 'si')
{
    $consulta_productos = $conexion->query("SELECT * FROM productos WHERE impuesto = '$id'");
    if ($consulta_productos->num_rows != 0)
    {
        $mensaje = "<p class='mensaje_error'>No es posible eliminar el impuesto <strong>$impuesto</strong> por que aún tiene productos relacionados a él.</p>";
    }
    else
    {
        $borrar = $conexion->query("DELETE FROM impuestos WHERE id = '$id'");
        if ($borrar)
        {
            $mensaje = "<p class='mensaje_exito'>El impuesto <strong>$impuesto</strong> fue eliminado exitosamente.</p>";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#financiero"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/impuestos.jpg');"></div>
            <h2 class="cab_texto">Impuestos</h2>
            <div class="bloque_margen">
                <p>Los impuestos son todas las obligaciones tributarias que tus productos o servicios generan, por ejemplo: IVA, impuesto gastronómico, impuesto de licores, etc. En esta sección puedes agregar, modificar y eliminar los los impuestos.</p>
                <p class="alineacion_botonera"><a href="impuestos_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo impuesto"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Impuestos agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un impuesto" /></p>                    
                </form>
                <?php
                //consulto y muestro los impuestos
                $consulta = $conexion->query("SELECT * FROM impuestos WHERE impuesto like '%$busqueda%' or porcentaje like '%$busqueda%' ORDER BY impuesto");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado impuestos para esta búsqueda.</p>

                    <?php
                }
                else                 
                {
                    ?>

                    <p>Toca una impuesto para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $impuesto = $fila['impuesto'];
                        $porcentaje = $fila['porcentaje'];

                        $consulta_productos = $conexion->query("SELECT * FROM productos WHERE impuesto = '$id'");
                        $total_productos = $consulta_productos->num_rows;
                        ?>
                        <a href="impuestos_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('img/iconos/impuestos.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$impuesto"); ?></span>
                                        <span class="item_descripcion">Porcentaje: <?php echo ucfirst("$porcentaje"); ?> %</span>
                                        <span class="item_descripcion">Productos: <?php echo ucfirst("$total_productos"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }                   
                }
                //fin consulto y muestro los impuestos
                ?>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>