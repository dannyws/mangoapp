<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$mensaje_com = isset($_GET['mensaje_com']) ? $_GET['mensaje_com'] : null ;

$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
$zona = isset($_POST['zona']) ? $_POST['zona'] : null ;
$producto = isset($_POST['producto']) ? $_POST['producto'] : null ;
$precio = isset($_POST['precio']) ? $_POST['precio'] : null ;
$impuesto = isset($_POST['impuesto']) ? $_POST['impuesto'] : null ;
$codigo_barras = isset($_POST['codigo_barras']) ? $_POST['codigo_barras'] : null ;
$descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : null ;
?>

<?php
//agregar el producto
if ($agregar == 'si')
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
    }
    else
    {
        $imagen = "no";
    }

    $consulta = $conexion->query("SELECT * FROM productos WHERE categoria = '$categoria' and producto = '$producto' LIMIT 1");

    if ($consulta->num_rows == 0)
    {
        $imagen_ref = "productos";
        $insercion = $conexion->query("INSERT INTO productos values ('', '$ahora', '$sesion_id', '$categoria', '$tipo', '$local', '$zona', '$producto', '$precio', '$impuesto', '$descripcion', '$codigo_barras', '$imagen', '$ahora_img')");
        $mensaje = "<p class='mensaje_exito'>El producto <strong>$producto</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');

        //agrego el componente
        if ($tipo == 'simple')
        {
            $insercion = $conexion->query("INSERT INTO componentes values ('', '$ahora', '$sesion_id', 'unid', '$producto', '$precio', '0')");
            $componente_id = $conexion->insert_id;
        }

        //agrego la composicion
        if ($tipo == 'simple')
        {
            $insercion = $conexion->query("INSERT INTO composiciones values ('', '$ahora', '$sesion_id', '$id', '$componente_id', '1')");
        }
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El producto <strong>$producto</strong> ya existe, no es posible agregarlo de nuevo. $mensaje_com</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="productos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Productos</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo producto</h2>
                <?php echo "$mensaje"; ?>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="image" />
                    <p><label for="categoria">Categoría:</label></p>
                    <p><select id="categoria" name="categoria" required autofocus>
                        <option value=""></option>
                        <?php
                        //consulto y muestro las categorias
                        $consulta = $conexion->query("SELECT * FROM productos_categorias ORDER BY categoria");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_categoria = $fila['id'];
                                $categoria = $fila['categoria'];
                                ?>

                                <option value="<?php echo "$id_categoria"; ?>"><?php echo ucfirst($categoria) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado categorías</option>

                            <?php
                        }
                        ?>
                    </select></p>


                    <p><label for="tipo">Tipo:</label></p>
                    <p><select id="tipo" name="tipo" required>
                        <option value=""></option>
                        <option value="compuesto">Compuesto</option>
                        <option value="simple">Simple</option>                        
                    </select></p>

                    <p><label for="local">Local en que se vende:</label></p>
                    <p><select id="local" name="local" required>
                        <option value=""></option>
                        <option value="0">Todos los locales</option>
                        <?php
                        //consulto y muestro los locales
                        $consulta = $conexion->query("SELECT * FROM locales ORDER BY local");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_local = $fila['id'];
                                $local = $fila['local'];
                                $tipo = $fila['tipo'];
                                ?>

                                <option value="<?php echo "$id_local"; ?>"><?php echo ucfirst($local) ?> (<?php echo ucfirst($tipo) ?>)</option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="0">Todos los locales</option>
                            <option value="">No se han agregado locales</option>

                            <?php
                        }
                        ?>
                    </select></p>

                    <p><label for="zona">Zona de entrega en el local:</label></p>
                    <p><select id="zona" name="zona" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro las zonas de entregas
                        $consulta = $conexion->query("SELECT * FROM zonas_entregas");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_zona = $fila['id'];
                                $zona = $fila['zona'];                                
                                ?>

                                <option value="<?php echo "$id_zona"; ?>"><?php echo ucfirst($zona) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>

                            <option value="">No se han agregado zonas de entregas</option>

                            <?php
                        }
                        ?>
                    </select></p>

                    <p><label for="producto">Producto:</label></p>
                    <p><input type="text" id="producto" name="producto" required /></p>
                    <p><label for="precio">Precio final de venta:</label></p>
                    <p><input type="number" id="precio" name="precio" required /></p>
                    <p><label for="impuesto">Impuesto:</label></p>
                    <p><select id="impuesto" name="impuesto" required>
                        <option value=""></option>
                        <?php
                        //consulto y muestro los impuestos
                        $consulta = $conexion->query("SELECT * FROM impuestos ORDER BY porcentaje");

                        if (!($consulta->num_rows == 0))
                        {
                            while ($fila = $consulta->fetch_assoc()) 
                            {
                                $id_impuesto = $fila['id'];
                                $impuesto = $fila['impuesto'];
                                $porcentaje = $fila['porcentaje'];
                                ?>

                                <option value="<?php echo "$id_impuesto"; ?>"><?php echo "$porcentaje"; ?> % - <?php echo ucfirst($impuesto) ?></option>

                                <?php
                            }
                        }
                        else
                        {
                            ?>
                            
                            <option value="">No se han agregado impuestos</option>

                            <?php
                        }
                        ?>
                    </select></p>
                    <p><label for="descripcion">Descripción (opcional):</label></p>
                    <p><textarea id="descripcion" name="descripcion"></textarea></p>
                    <p><label for="codigo_barras">Código de barras (opcional):</label></p>
                    <p><input type="number" id="codigo_barras" name="codigo_barras" /></p>
                    <p><label for="archivo">Imagen: </label></p>
                    <p><input type="file" id="archivo" name="archivo" /></p>
                    <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
                </form>
            </div>
        </article>
    </section>

        <footer></footer>
</body>
</html>