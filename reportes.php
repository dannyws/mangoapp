<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head <meta http-equiv="refresh" content="20" >
    ?>    
</head>
<body>

    <header>
        <div class="header_contenedor">
            <a href="index.php">
                <div class="cabezote_col_izq">
                    <h2><div class="flecha_izq"></div> <span class="logo_txt">Inicio</span></h2>
                </div>
            </a>
            <a href="index.php">
                <div class="cabezote_col_cen">
                    <h2><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></h2>
                </div>
            </a>
            <a href="ajustes.php">
                <div class="cabezote_col_der">
                    <h2></h2>
                </div>
            </a>
        </div>
    </header>














    <section id="contenedor">



        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/reportes.jpg');"></div>
            <div class="bloque_margen">
                <h2>Reportes</h2>
                <p>Acá podrás consultar la información que tus negocios generan en los periodos de tiempo que necesites ver.</p>
            </div>
        </article>



        <article class="bloque">
            <div class="bloque_margen">
                <h2>Financieros</h2>
                <a href="reportes_ingresos.php?rango=hoy">
                    <div class="item">
                        <div class="item">
                            <div class="item_img">
                                <div class="img_avatar" style="background-image: url('img/iconos/canje.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Ingresos</span>
                                <span class="item_descripcion">Consulta el total de dinero que ingresa en cada uno de tus locales.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <a href="reportes_productos.php?rango=hoy">
                    <div class="item">
                        <div class="item">
                            <div class="item_img">
                                <div class="img_avatar" style="background-image: url('img/iconos/productos.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Contador de productos</span>
                                <span class="item_descripcion">Consulta el número de productos mas vendidos.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>
                
                <a href="reportes_ventas.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img">
                                <div class="img_avatar" style="background-image: url('img/iconos/caja_libre.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Contador de ventas</span>
                                <span class="item_descripcion">Consulta el número de ventas en cada uno de tus locales.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>

                <a href="reportes_tipos_pagos.php">
                    <div class="item">
                        <div class="item">
                            <div class="item_img">
                                <div class="img_avatar" style="background-image: url('img/iconos/tarjeta.jpg');"></div>
                            </div>
                            <div class="item_info">
                                <span class="item_titulo">Tipos de pago</span>
                                <span class="item_descripcion">Consulta la cantidad de dinero que genera cada tipo de pago.</span>
                                
                            </div>
                        </div>
                    </div>
                </a>
                
            </div>
        </article>
        

    </section>

    <footer></footer>

</body>
</html>