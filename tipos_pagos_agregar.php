<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion, sesion y subida
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$agregar = isset($_POST['agregar']) ? $_POST['agregar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;

$tipo_pago = isset($_POST['tipo_pago']) ? $_POST['tipo_pago'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
?>

<?php
//agregar el tipo de pago
if ($agregar == 'si')
{
    $consulta = $conexion->query("SELECT * FROM tipos_pagos WHERE tipo_pago = '$tipo_pago' and tipo = '$tipo'");

    if ($consulta->num_rows == 0)
    {
        $insercion = $conexion->query("INSERT INTO tipos_pagos values ('', '$ahora', '$sesion_id', '$tipo_pago', '$tipo')");
        $mensaje = "<p class='mensaje_exito'>El tipo de pago <strong>$tipo_pago</strong> fue agregado exitosamente.</p>";

        $id = $conexion->insert_id;
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>El tipo de pago <strong>$tipo_pago</strong> ya existe, no es posible agregarlo de nuevo.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>
    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="tipos_pagos_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Tipos de pago</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>
    <section id="contenedor">       
        <article class="bloque">
            <div class="bloque_margen">
                <h2>Agregar un nuevo tipo de pago</h2>
                <?php echo "$mensaje"; ?>                
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <p><label for="tipo_pago">Tipo de pago:</label></p>
                <p><input type="text" id="tipo_pago" name="tipo_pago" required autofocus /></p>                
                <p><label for="tipo">Tipo:</label></p>
                <p><select id="tipo" name="tipo" required>
                    <option value=""></option>
                    <option value="canje">Canje</option>
                    <option value="cheque">Cheque</option>
                    <option value="efectivo">Efectivo</option>
                    <option value="tarjeta">Tarjeta</option>
                </select></p>                
                <p class="alineacion_botonera"><button type="submit" class="proceder" name="agregar" value="si">Guardar cambios</button></p>
            </form>
            </div>
        </article>
    </section>
    <footer></footer>
</body>
</html>