<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
?>

<?php
//capturo las variables que pasan por URL
$eliminar = isset($_GET['eliminar']) ? $_GET['eliminar'] : null ;
$componente = isset($_GET['componente']) ? $_GET['componente'] : null ;
$id = isset($_GET['id']) ? $_GET['id'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : null ;
?>

<?php
//elimino el componente
if ($eliminar == "si")
{
    $borrar = $conexion->query("DELETE FROM componentes WHERE id = '$id'");

    if ($borrar)
    {
        $mensaje = "<p class='mensaje_exito'>El componente <strong>$componente</strong> fue eliminado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_exito'>No es posible eliminar el componente <strong>$componente</strong>.</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="ajustes.php#inventario"><div class="flecha_izq"></div> <span class="logo_txt"> Ajustes</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">

        <article class="bloque">
            <div class="img_arriba_ajustes" style="background-image: url('img/sis/componentes.jpg');"></div>
            <h2 class="cab_texto">Componentes</h2>
            <div class="bloque_margen">
                <p>Los componentes son todos los elementos de los que están compuestos los productos o servicios que vendes, por ejemplo: una hamburguesa está hecha de pan, carne y lechuga.</p>
                <p class="alineacion_botonera"><a href="componentes_agregar.php"><input type="button" class="proceder" value="Agregar un nuevo componente"></a></p>
                <?php echo "$mensaje"; ?>
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Componentes agregados</h2>
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">                    
                    <p><input type="text" name="busqueda" value="<?php echo "$busqueda"; ?>" placeholder="Buscar un componente" /></p>                    
                </form>
                <?php
                //consulto el proveedor previamente para la busqueda
                $consulta0 = $conexion->query("SELECT * FROM proveedores WHERE proveedor like '%$busqueda%'");

                if ($filas0 = $consulta0->fetch_assoc())
                {
                    $proveedor = $filas0['id'];
                }
                else
                {
                    $proveedor = "bullshit";
                }

                //consulto y muestro los componentes
                $consulta = $conexion->query("SELECT * FROM componentes WHERE componente like '%$busqueda%' or proveedor like '%$proveedor%' ORDER BY proveedor, componente");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado componentes para esta búsqueda.</p>

                    <?php
                }
                else
                {
                    ?>

                    <p>Toca un componente para verlo o editarlo.</p>

                    <?php
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $unidad = $fila['unidad'];
                        $componente = $fila['componente'];
                        $precio_unidad = $fila['precio_unidad'];
                        $proveedor = $fila['proveedor'];

                         //consulto el proveedor
                        $consulta2 = $conexion->query("SELECT * FROM proveedores WHERE id = $proveedor");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $proveedor = $filas2['proveedor'];
                        }
                        else
                        {
                            $proveedor = "No se ha asignado un proveedor";
                        }                      
                        ?>
                        <a href="componentes_detalle.php?id=<?php echo "$id"; ?>">
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/componentes.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$componente"); ?></span>
                                        <span class="item_descripcion">Proveedor: <?php echo ucfirst("$proveedor"); ?></span>
                                        <span class="item_descripcion">Precio unidad: $<?php echo number_format($precio_unidad, 0, ",", "."); ?> x <?php echo ucfirst("$unidad"); ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </article>  

    </section>
    <footer></footer>
</body>
</html>