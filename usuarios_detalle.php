<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;

$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : null ;
$imagen_nombre = isset($_POST['imagen_nombre']) ? $_POST['imagen_nombre'] : null ;
$correo = isset($_POST['correo']) ? $_POST['correo'] : null ;
$contrasena = isset($_POST['contrasena']) ? $_POST['contrasena'] : null ;
$nombres = isset($_POST['nombres']) ? $_POST['nombres'] : null ;
$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;

$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//actualizo la información del usuario
if ($editar == "si")
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
        $imagen_nombre = $ahora_img;
        $imagen_ref = "usuarios";

        //si han cargado el archivo subimos la imagen
        include('imagenes_subir.php');
    }
    else
    {
        $imagen = $imagen;
        $imagen_nombre = $imagen_nombre;
    }

    $actualizar = $conexion->query("UPDATE usuarios SET fecha = '$ahora', usuario = '$sesion_id', correo = '$correo', contrasena = '$contrasena', nombres = '$nombres', apellidos = '$apellidos', tipo = '$tipo', local = '$local', imagen = '$imagen', imagen_nombre = '$imagen_nombre' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El usuario <strong>$correo</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el usuario.";
    }        
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="usuarios_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Usuarios</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">
        <article class="bloque">
            <?php
            //consulto y muestro el usuario
            $consulta = $conexion->query("SELECT * FROM usuarios WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Usuario eliminado</h2>
                    <p class="mensaje_error">Este usuario ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $correo = $fila['correo'];
                    $contrasena = $fila['contrasena'];
                    $nombres = $fila['nombres'];
                    $apellidos = $fila['apellidos'];
                    $tipo = $fila['tipo'];
                    $local = $fila['local'];
                    $imagen = $fila['imagen'];
                    $imagen_nombre = $fila['imagen_nombre'];

                    if ($imagen == "no")
                    {
                        $imagen = "img/iconos/usuarios.jpg";
                    }
                    else
                    {
                        $imagen = "img/avatares/usuarios-$id-$imagen_nombre.jpg";
                    }

                    //consulto el local
                    $consulta_local = $conexion->query("SELECT * FROM locales WHERE id = '$local'");           

                    if ($fila = $consulta_local->fetch_assoc()) 
                    {
                        $local = $fila['local'];
                        $local_tipo = $fila['tipo'];
                    }
                    else
                    {
                        $local = "No se ha asignado un local";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    
                    ?>

                    <div class="img_arriba" style="background-image: url('<?php echo ($imagen) ?>');"></div>
                    <h2 class="cab_texto"><?php echo ucwords("$nombres"); ?> <?php echo ucwords("$apellidos"); ?></h2>
                    <div class="bloque_margen">
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Correo</span><?php echo ($correo) ?></p>
                        <p><span class="item_titulo">Tipo</span><?php echo ucfirst($tipo) ?></p>
                        <p><span class="item_titulo">Local</span><?php echo ucfirst($local) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>
                        <p class="alineacion_botonera"><a href="usuarios_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este usuario"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>
        </article>
    </section>
    <footer></footer>
</body>
</html>