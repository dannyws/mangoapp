<?php
//inicio la sesión
session_start();

//verifico si la sesión está creada y si no lo está lo envio al logueo
if (!isset($_SESSION['correo']))
{
    header("location:logueo.php");
}
?>

<?php
//variables de la conexion y de sesion
include ("sis/conexion.php");
include ("sis/variables_sesion.php");
include('sis/subir.php');
?>

<?php
//capturo las variables que pasan por URL
$editar = isset($_POST['editar']) ? $_POST['editar'] : null ;
$mensaje = isset($_GET['mensaje']) ? $_GET['mensaje'] : null ;
$id = isset($_POST['id']) ? $_POST['id'] : null ;

$id_url = isset($_GET['id']) ? $_GET['id'] : null ;

$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : null ;
$imagen_nombre = isset($_POST['imagen_nombre']) ? $_POST['imagen_nombre'] : null ;
$local = isset($_POST['local']) ? $_POST['local'] : null ;
$direccion = isset($_POST['direccion']) ? $_POST['direccion'] : null ;
$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : null ;
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null ;
$archivo = isset($_POST['archivo']) ? $_POST['archivo'] : null ;

$carpeta_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'img/avatares');
$dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $carpeta_destino);
?>

<?php
//actualizo la información del local
if ($editar == "si")
{
    if (!(isset($archivo)) && ($_FILES['archivo']['type'] == "image/jpeg") || ($_FILES['archivo']['type'] == "image/png"))
    {
        $imagen = "si";
        $imagen_nombre = $ahora_img;
        $imagen_ref = "locales";

        //si han cargado el archivo subimos la imagen        
        include('imagenes_subir.php');
    }
    else
    {
        $imagen = $imagen;
        $imagen_nombre = $imagen_nombre;
    }

    $actualizar = $conexion->query("UPDATE locales SET fecha = '$ahora', usuario = '$sesion_id', local = '$local', direccion = '$direccion', telefono = '$telefono', tipo = '$tipo', imagen = '$imagen', imagen_nombre = '$imagen_nombre' WHERE id = '$id'");

    if ($actualizar)
    {
        $mensaje = "<p class='mensaje_exito'>El local <strong>$local</strong> fue modificado exitosamente.</p>";
    }
    else
    {
        $mensaje = "<p class='mensaje_error'>No se pudo modificar el local.";
    }        
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>ManGo!</title>    
    <?php
    //información del head
    include ("partes/head.php");
    //fin información del head
    ?>
</head>
<body>

    <header>
        <div class="header_contenedor">
            <div class="cabezote_col_izq">
                <h2><a href="locales_ver.php"><div class="flecha_izq"></div> <span class="logo_txt"> Locales</span></a></h2>
            </div>
            <div class="cabezote_col_cen">
                <h2><a href="index.php"><div class="logo_img"></div> <span class="logo_txt">ManGo!</span></a></h2>
            </div>
            <div class="cabezote_col_der">
                <h2></h2>
            </div>
        </div>
    </header>

    <section id="contenedor">
        <article class="bloque">
            <?php
            //consulto y muestro el local
            $consulta = $conexion->query("SELECT * FROM locales WHERE id = '$id' or id = '$id_url'");

            if ($consulta->num_rows == 0)
            {
                ?>

                <div class="bloque_margen">
                    <h2>Local eliminado</h2>
                    <p class="mensaje_error">Este local ya no existe.</p>
                </div>

                <?php
            }
            else             
            {
                while ($fila = $consulta->fetch_assoc())
                {
                    $id = $fila['id'];
                    $fecha = date('d M', strtotime($fila['fecha']));
                    $hora = date('h:i a', strtotime($fila['fecha']));
                    $usuario = $fila['usuario'];
                    $local = $fila['local'];
                    $direccion = $fila['direccion'];
                    $telefono = $fila['telefono'];
                    $tipo = $fila['tipo'];
                    $imagen = $fila['imagen'];
                    $imagen_nombre = $fila['imagen_nombre'];

                    if ($imagen == "no")
                    {
                        $imagen = "img/iconos/locales.jpg";
                    }
                    else
                    {
                        $imagen = "img/avatares/locales-$id-$imagen_nombre.jpg";
                    }

                    //consulto el usuario que realizo la ultima modificacion
                    $consulta_usuario = $conexion->query("SELECT * FROM usuarios WHERE id = '$usuario'");           

                    if ($fila = $consulta_usuario->fetch_assoc()) 
                    {
                        $usuario = $fila['correo'];
                    }
                    ?>

                    <div class="img_arriba" style="background-image: url('<?php echo ($imagen) ?>');"></div>
                    <h2 class="cab_texto"><?php echo ucfirst("$local"); ?></h2>
                    <div class="bloque_margen">                        
                        <?php echo "$mensaje"; ?>
                        <p><span class="item_titulo">Dirección</span><?php echo ucfirst($direccion) ?></p>
                        <p><span class="item_titulo">Teléfono</span><?php echo ucfirst($telefono) ?></p>
                        <p><span class="item_titulo">Tipo</span><?php echo ucfirst($tipo) ?></p>
                        <p><span class="item_titulo">Última modificación</span><?php echo ucfirst("$fecha"); ?> - <?php echo ucfirst("$hora"); ?> / <?php echo ("$usuario"); ?></p>                    
                        <p class="alineacion_botonera"><a href="locales_editar.php?id=<?php echo "$id"; ?>"><input type="button" class="proceder" value="Modificar este local"></a></p>
                    </div>
                    
                    <?php
                }
            }
            ?>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Usuarios relacionados a este local</h2>                
                <?php
                //consulto los usuarios
                $consulta = $conexion->query("SELECT * FROM usuarios WHERE local = '$id' ORDER BY nombres");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado usuarios relacionados a este local.</p>

                    <?php
                }
                else
                {   
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id_usuario = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i a', strtotime($fila['fecha']));
                        $correo = $fila['correo'];
                        $contrasena = $fila['contrasena'];
                        $nombres = $fila['nombres'];
                        $apellidos = $fila['apellidos'];
                        $tipo = $fila['tipo'];
                        $local = $fila['local'];
                        $imagen = $fila['imagen'];
                        $imagen_nombre = $fila['imagen_nombre'];

                        if ($imagen == "no")
                        {
                            $imagen = "img/iconos/usuarios-m.jpg";
                        }
                        else
                        {
                            $imagen = "img/avatares/usuarios-$id_usuario-$imagen_nombre-m.jpg";
                        } 
                        ?>
                        
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">
                                        <div class="img_avatar" style="background-image: url('<?php echo "$imagen";?>');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucwords("$nombres"); ?> <?php echo ucwords("$apellidos"); ?></span>
                                        <span class="item_descripcion">Tipo: <?php echo ucfirst("$tipo"); ?></span>                                       
                                    </div>
                                </div>
                            </div>
                        
                        <?php
                    }
                }
                ?>
                
            </div>
        </article>

        <article class="bloque">
            <div class="bloque_margen">
                <h2>Ubicaciones relacionadas a este local</h2>                
                <?php
                //consulto y muestro las ubicaciones
                $consulta = $conexion->query("SELECT * FROM ubicaciones WHERE local = '$id' ORDER BY ubicada, ubicacion");

                if ($consulta->num_rows == 0)
                {
                    ?>

                    <p class="mensaje_error">No se han encontrado ubicaciones relacionadas a este local.</p>

                    <?php
                }
                else
                {
                    while ($fila = $consulta->fetch_assoc())
                    {
                        $id = $fila['id'];
                        $fecha = date('d M', strtotime($fila['fecha']));
                        $hora = date('h:i:s a', strtotime($fila['fecha']));
                        $ubicacion = $fila['ubicacion'];
                        $ubicada = $fila['ubicada'];
                        $estado = $fila['estado'];
                        $tipo = $fila['tipo'];
                        $local = $fila['local'];

                        //consulto el local
                        $consulta2 = $conexion->query("SELECT * FROM locales WHERE id = $local");

                        if ($filas2 = $consulta2->fetch_assoc())
                        {
                            $local = $filas2['local'];
                            $local_tipo = $filas2['tipo'];
                        }
                        else
                        {
                            $local = "No se ha asignado un local";
                            $local_tipo = "--";
                        }                      
                        ?>
                        
                            <div class="item">
                                <div class="item">
                                    <div class="item_img">                            
                                        <div class="img_avatar" style="background-image: url('img/iconos/<?php echo "$tipo"; ?>_<?php echo "$estado"; ?>.jpg');"></div>
                                    </div>
                                    <div class="item_info">
                                        <span class="item_titulo"><?php echo ucfirst("$ubicacion"); ?></span>
                                        <span class="item_descripcion">Ubicada en: <?php echo ucfirst("$ubicada"); ?></span>                                        
                                    </div>
                                </div>
                            </div>
                        
                        <?php
                    }
                }
                ?>
            </div>
        </article>

    </section>
    <footer></footer>
</body>
</html>